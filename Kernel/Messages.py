#!/usr/bin/env python
# -*- coding: utf-8 -*-

training_w2v = "* Training w2v model..."
loading_sentences = "* Loading sentences..."
loading_model     = "* Loading w2v model..."
preprocessing_sentences = "* Preprocessing sentences..."
extracting_features = "* Extracting features froms sentences..."
extracting_clustering_classes  = "* Extracting classes using clustering..."
extracted_classes   = "* Classes extracted using clustering: "
predicted_class      = "* Predicted class: "
testing_file_header  = "----- Testing file -----\n\n" 
clustering_header   = "------ CLUSTERING ------\n\n"
supervised_header   = "------ SUPERVISED ------\n\n"
statistics_supervised_header = "------ SUPERVISED STATISTICS ------\n\n"
statistics_clustering_header = "------ CLUSTERING STATISTICS ------\n\n"
statistics_lou_header = "------ LEAVING ONE OUT STATISTICS ------\n\n"
statistics_resustitution_header = "------ RESUSTITUTION STATISTICS ------\n\n"
statistics_partition_header = "------ PARTITION STATISTICS ------\n\n"
statistics_common_header = "------ COMMON STATISTICS ------\n\n"

