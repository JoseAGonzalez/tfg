#!/usr/bin/env python
# -*- coding: utf-8 -*-
import Config

def extract_continuous_polarity_from_list(test_terms,pos,neg,model):
    res = []
    for test_term in test_terms: res.append((test_term,get_polarity_w2v(test_term,pos,neg,model)))
    return res

def extract_binary_polarity_from_list(test_terms,pos,neg,model):
    res = []
    for test_term in test_terms: res.append((test_term,"positive" if get_polarity_w2v(test_term,pos,neg,model)>=0 else "negative"))
    return res

def extract_continuous_polarity_from_list_distance(test_terms,pos,neg,model,distance):
    res = []
    for test_term in test_terms: res.append((test_term,get_polarity_distance(test_term,pos,neg,model,distance)))
    return res

def extract_binary_polarity_from_list_distance(test_terms,pos,neg,model):
    res = []
    for test_term in test_terms: res.append((test_term,"positive" if get_polarity_distance(test_term,pos,neg,model,distance)>=0 else "negative"))
    return res

def convert_continuous_polarity_to_binary(test_res):
    for i in xrange(len(test_res)):
	if test_res[i][1]>=0: test_res[i][1] = "positive"
	else:                 test_res[i][1] = "negative"
    return test_res
    
def get_polarity_w2v(term,pos,neg,model):
    sim_pos,sim_neg = 0,0
    for pos_term in pos: 
	try:
	    sim_pos += model.similarity(term,pos_term)
	except Exception as e: continue
    for neg_term in neg: 
	try:
	    sim_neg += model.similarity(term,neg_term)
	except Exception as e: continue
    return (1.0/len(pos))*sim_pos - (1.0/len(neg))*sim_neg

def get_polarity_distance(term,pos,neg,model,distance):
    sim_pos,sim_neg = 0,0
    for pos_term in pos: 
	try:
	    sim_pos += 1-distance(model[term],model[pos_term])
	except Exception as e: continue
    for neg_term in neg: 
	try:
	    sim_neg += 1-distance(model[term],model[neg_term])
	except Exception as e: continue
    return (1.0/len(pos))*sim_pos - (1.0/len(neg))*sim_neg
