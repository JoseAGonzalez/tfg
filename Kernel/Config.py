#!/usr/bin/env python
# -*- coding: utf-8 -*-

from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from nltk.stem   import WordNetLemmatizer
from scipy.spatial import distance
#from FeatureExtractor import get_sentence_representation_sum, get_sentence_representation_mult,get_sentence_representation_distance,get_sentence_representation_weighted,get_sentence_representation_covariance
import FeatureExtractor,FeatureExtractorDBpediaProject,FeatureExtractorDBpediaSpotlight
from sklearn import svm
from sklearn.neighbors.nearest_centroid import NearestCentroid
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB,BernoulliNB,MultinomialNB
from sklearn import tree
from sklearn.linear_model import SGDClassifier
from sklearn.cluster import KMeans,AgglomerativeClustering,DBSCAN,Birch
from sklearn.ensemble import RandomForestClassifier,AdaBoostClassifier,BaggingClassifier
from NeighboursClassifier import get_class
from NeighboursClassifier import get_k_classes
from SPARQLWrapper import SPARQLWrapper, JSON
from Utils import unserialize


## Project info ##

AUTHOR   = "Jose A. Gonzalez"
VERSION  = ""

################

## General ##

VERBOSE = False
REPORT  = False

#############

## Preprocess ##

LANGUAGES = ["spanish","english"]
LANGUAGE = "spanish"
STOPWORDS      = []
STOPWORDS_FILE = []
# Configure NLTK stopwords #
for lang in LANGUAGES: 
    for word in stopwords.words(lang): STOPWORDS.append(word)
# Configure FILE stopwords #
#fd = open("../Kernel/stopwords/stopwords_es.txt","r")
#for stopwords in fd.readlines(): STOPWORDS_FILE.append(stopwords[:-1])
#fd.close()
#######################
STEMMER   = SnowballStemmer(LANGUAGE)
LEMMATIZER = WordNetLemmatizer()
PATTERN_TOKENIZER = " "
PATTERN_SEP_SENTENCE  = "\n"
NON_ALFANUMERIC_PATTERNS = ["\n",".",",","?","!","\\","/",";","'","\"",":",u"¿",u"¡",u"-",u"_",u"(",u")",u"[",u"]",u"{",u"}"]
NON_ALFANUMERIC_FUNC     = 1

###############

############################################ Learning ########################################################

## Feature and classification ##
DISTANCES = {"BRAYCURTIS":distance.braycurtis,
	     "CANBERRA":distance.canberra,
	     "CHEBYSHEV":distance.chebyshev,
			 "CITYBLOCK":distance.cityblock,
			 "CORRELATION":distance.correlation,
			 "COSINE":distance.cosine,
			 "DICE":distance.dice,
			 "EUCLIDEAN":distance.euclidean,
			 "HAMMING":distance.hamming,
			 "JACCARD":distance.jaccard,
			 "KULSINSKI":distance.kulsinski,
		     "MATCHING":distance.matching,
		     "ROGERSTANIMOTO":distance.rogerstanimoto,
		     "SOKALMICHENER":distance.sokalmichener,
		     "SOKALSNEATH":distance.sokalsneath,
		     "SQEUCLIDEAN":distance.sqeuclidean}
		     
## Supervised classification ##

DISTANCE_CLASSIFICATION = DISTANCES["CORRELATION"]
K_PROJECT_CLASSIFIER    = 1

## Clustering classification ##
N_CLUSTERS = 73

# Classifiers (configure them in scripts) #
## Todas las pruebas donde ponga KNN es NearestCentroid!! #
SUPERVISED_CLASSIFIERS   = {"BAGGING":BaggingClassifier,"ADA_BOOST":AdaBoostClassifier,"RANDOM_FOREST":RandomForestClassifier,"SVM_KERNEL":svm.SVC,"SVM_LINEAR":svm.LinearSVC,"KNN":KNeighborsClassifier,"NEAREST_CENTROID":NearestCentroid,"GAUSSIAN":GaussianNB,"BERNOULLI":BernoulliNB,"MULTINOMIAL":MultinomialNB,"DECISION_TREE":tree.DecisionTreeClassifier}
CLUSTERING_CLASSIFIERS = {"KMEANS":KMeans,"AGGLOMERATIVE":AgglomerativeClustering,"DBSCAN":DBSCAN,"BIRCH":Birch}
SUPERVISED_CLASSIFIERS_FOR_TEST = {"RANDOM_FOREST_50":RandomForestClassifier(n_estimators=50,n_jobs=8),
				   "BERNOULLI":BernoulliNB(),
				   "GAUSSIAN":GaussianNB(),
				   "SVM_KERNEL_LINEAR":svm.LinearSVC(),
				   "DECISION_TREE":tree.DecisionTreeClassifier(),
				   "NEAREST_CENTROID_COSINE":NearestCentroid(metric="cosine"),
				   "NEAREST_CENTROID_CORRELATION":NearestCentroid(metric="correlation"),
				   "KNN_1":KNeighborsClassifier(n_neighbors=1),
				   "RANDOM_FOREST_100":RandomForestClassifier(n_estimators=100,n_jobs=8),
				   "SVM_KERNEL_RBF":svm.SVC(kernel='rbf',gamma='auto')
				  }
				  
SUPERVISED_CLASSIFIERS_FOR_TUNING = {"KNN_1":KNeighborsClassifier(n_neighbors=1),
				     "SVM_KERNEL_LINEAR":svm.LinearSVC(),
				     "RANDOM_FOREST_100":RandomForestClassifier(n_estimators=100,n_jobs=8)
				     }
				 # "RANDOM_FOREST_10":RandomForestClassifier(n_estimators=10,n_jobs=8),
				 # "SVM_KERNEL_LINEAR":svm.LinearSVC(),
				 # "KNN_2":KNeighborsClassifier(n_neighbors=2)
				 # "RANDOM_FOREST_100":RandomForestClassifier(n_estimators=100,n_jobs=8), 
				 # "SVM_SOFT_MARGIN":svm.SVC(C=0.5),
				 # "SVM_KERNEL_RBF":svm.SVC(kernel='rbf',gamma='auto'),
#SUPERVISED_CLASSIFIER   = SUPERVISED_CLASSIFIERS["SVM_LINEAR"]
#SUPERVISED_CLASSIFIER_FOR_TEST = SUPERVISED_CLASSIFIERS_FOR_TEST["GAUSSIAN"]
CLUSTERING_CLASSIFIER   = CLUSTERING_CLASSIFIERS["KMEANS"]
####################		     

############################################ End Learning ########################################################


############################################# DBpedia Project model ##################################################

## General ##
SPARQL_PREFIX   = "<http://es.dbpedia.org/resource/>"
SPARQL_ENDPOINT = SPARQLWrapper("http://es.dbpedia.org/sparql")
SPARQL_ENDPOINT.setReturnFormat(JSON)
#DBCLASSES = unserialize("../TopicDetection/classes_hash.dump")[0] 
DBPROPERTIES = None # Cargar cuando el servidor no este caido 

## End General ##

## Feature extraction ##
SENTENCE_REPRESENTATION_DBPEDIA = FeatureExtractorDBpediaProject.get_sentence_representation_sum
DISTANCE_REPRESENTATION_DBPEDIA = DISTANCES["COSINE"]
REGULARIZATION_DBPEDIA 		= False
## End Feature extraction ##

############################################# End DBpedia Project model ##################################################

############################################# DBpedia Spotlight model ##################################################
SPOTLIGHT_SPARQL_PREFIX   = "http://es.dbpedia.org/resource/"
SPOTLIGHT_SPARQL_ENDPOINT = SPARQLWrapper("http://es.dbpedia.org/sparql")
SPOTLIGHT_SPARQL_ENDPOINT.setReturnFormat(JSON)
SPOTLIGHT_CONFIDENCE = 0.35
SPOTLIGHT_ENDPOINT = "http://spotlight.sztaki.hu:2231/rest/annotate"
SPOTLIGHT_APPLICATION = "application/json"
SENTENCE_REPRESENTATION_SPOTLIGHT_DBPEDIA = FeatureExtractorDBpediaSpotlight.get_sentence_representation_sum
DISTANCE_REPRESENTATION_SPOTLIGHT_DBPEDIA = DISTANCES["COSINE"]
REGULARIZATION_DBPEDIA_SPOTLIGHT	  = False
############################################# End DBpedia Spotlight model ##################################################

#############################################      W2V      ##################################################

## Word2vec model ##

MIN_COUNT_W2V = 3
WINDOW_W2V    = 150
SIZE_W2V      = 800
WORKERS_W2V   = 1
SG_W2V        = 1
HS_W2V        = 0
NEG_W2V       = 5

## Feature extraction ##

SENTENCE_REPRESENTATION = FeatureExtractor.get_sentence_representation_sum
DISTANCE_REPRESENTATION = DISTANCES["COSINE"]
REGULARIZATION          = False

#############################################     End W2V      ##################################################

#############################################     Graphics     ##################################################
COLOURS = ["bD","gD","rD","cD","mD","yD","kD","bs","gs","rs","cs","ms","ys","ks","b|","g|","r|","c|","m|","y|","k|","bx","gx","rx","cx","mx","yx","kx","b_","g_","r_","c_","m_","y_","k_","b^","g^","r^","c^","m^","y^","k^","bd","gd","rd","cd","md","yd","kd","bh","gh","rh","ch","mh","yh","kh","b+","g+","r+","c+","m+","y+","k+","b*","g*","r*","c*","m*","y*","k*","b,","g,","r,","c,","m,","y,","k,","bo","go","ro","co","mo","yo","ko","b.","g.","r.","c.","m.","y.","k.","b1","g1","r1","c1","m1","y1","k1","bp","gp","rp","cp","mp","yp","kp","b3","g3","r3","c3","m3","y3","k3","b2","g2","r2","c2","m2","y2","k2","b4","g4","r4","c4","m4","y4","k4","bH","gH","rH","cH","mH","yH","kH","bv","gv","rv","cv","mv","yv","kv","b8","g8","r8","c8","m8","y8","k8","b<","g<","r<","c<","m<","y<","k<","b>","g>","r>","c>","m>","y>","k>"]
