#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import Config
from gensim.models import Word2Vec
from NeighboursClassifier import get_class
from Utils import unzip_2d,classes_to_nn,normalize_norm_supervised_samples,normalize_norm_unsupervised_samples,normalize_length_unsupervised_samples,normalize_length_supervised_samples
from FeatureExtractor import get_sentences_representation_supervised,get_sentences_representation_unsupervised
from warnings import filterwarnings
from ffnet import mlgraph,ffnet
from numpy import where
# Filter warnings from other libs #
filterwarnings("ignore")
###################################


#################################### Partition ####################################

def partition_nearest(train_sentences,test_sentences,k=1,model=None):
	if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 
	if model==None:
		model = Word2Vec([sentence for (sentence,cat) in train_sentences],min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V)
	repr_train_sentences = get_sentences_representation_supervised(train_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
	repr_test_sentences  = get_sentences_representation_supervised(test_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
	err,i = 0,0
	for repr_test_sentence in repr_test_sentences:
		if Config.VERBOSE: logging.info("Testing test sentence: "+str(i))
		c_class = get_class(repr_test_sentence[0],repr_train_sentences,k)
		if Config.VERBOSE: logging.info("Sample class: "+str(c_class)+" and correct class is: "+repr_test_sentence[1])
		if c_class!=repr_test_sentence[1]: err += 1
		i += 1
	print "Accuracy: ",1.0-(float(err)/len(test_sentences))
	return (float(err)/len(test_sentences))

def partition_supervised(train_sentences,test_sentences,clf,model=None,fnormalization=None,train_lengths=None,test_lengths=None):
	if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 
	if model==None:
		model = Word2Vec([sentence for (sentence,cat) in train_sentences],min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V)
	repr_train_sentences = get_sentences_representation_supervised(train_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
	repr_test_sentences  = get_sentences_representation_supervised(test_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
	if fnormalization!=None:
	    if fnormalization==normalize_length_supervised_samples:
		repr_train_sentences = fnormalization(repr_train_sentences,train_lengths)
		repr_test_sentences  = fnormalization(repr_test_sentences,test_lengths)
	    else:
		repr_train_sentences = fnormalization(repr_train_sentences)
		repr_test_sentences  = fnormalization(repr_test_sentences)
	err,i = 0,0
	# Training #
	h,s,c = {},set([cat for (sentence,cat) in repr_train_sentences]),0
	for cat in s: h[cat] = c; c += 1
	X,Y = [],[]
	for (repr_train_sentence,cat) in repr_train_sentences: 
		X.append(repr_train_sentence); 
		Y.append(h[cat])
	clf.fit(X,Y)
	for repr_test_sentence in repr_test_sentences:
		if Config.VERBOSE: logging.info("Testing test sentence: "+str(i))
		# Testing #
		y = clf.predict(repr_test_sentence[0]);
		if y[0]!=h[repr_test_sentence[1]]: err += 1
		if Config.VERBOSE: logging.info("Sample class: "+str(y[0])+" and correct class is: "+str(repr_test_sentence[1]))
		i += 1
	#print "Accuracy: ",1.0-(float(err)/len(repr_test_sentences))
	return 1.0-(float(err)/len(repr_test_sentences))

def partition_supervised_repr(repr_train_sentences,repr_test_sentences,clf,fnormalization=None,train_lengths=None,test_lengths=None):
	if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 
	err,i = 0,0
	if fnormalization!=None:
	    if fnormalization==normalize_length_supervised_samples:
		repr_train_sentences = fnormalization(repr_train_sentences,train_lengths)
		repr_test_sentences  = fnormalization(repr_test_sentences,test_lengths)
	    else:
		repr_train_sentences = fnormalization(repr_train_sentences)
		repr_test_sentences  = fnormalization(repr_test_sentences)
	# Training #
	h,s,c = {},set([cat for (sentence,cat) in repr_train_sentences]),0
	for cat in s: h[cat] = c; c += 1
	X,Y = [],[]
	for (repr_train_sentence,cat) in repr_train_sentences: 
		X.append(repr_train_sentence); 
		Y.append(h[cat])
	clf.fit(X,Y)
	for repr_test_sentence in repr_test_sentences:
	    if Config.VERBOSE: logging.info("Testing test sentence: "+str(i))
	    # Testing #
	    y = clf.predict(repr_test_sentence[0]);
	    if y[0]!=h[repr_test_sentence[1]]: err += 1
	    if Config.VERBOSE: logging.info("Sample class: "+str(y[0])+" and correct class is: "+str(repr_test_sentence[1]))
	    i += 1
	#print "Accuracy: ",1.0-(float(err)/len(repr_test_sentences))
	return 1.0-(float(err)/len(repr_test_sentences))
	
def partition_supervised_repr_neural_networks(repr_train_sentences,repr_test_sentences,graph,n_classes,fnormalization=None):
    if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 
    err,i = 0,0
    conec = mlgraph(graph)
    net   = ffnet(conec)
    ## Reemplazar clases por valores discretos ##
    h,s,c = {},set([cat for (sentence,cat) in repr_train_sentences]),0
    for cat in s: h[cat] = c; c += 1
    repr_train_sentences = [(s,h[c]) for (s,c) in repr_train_sentences]
    repr_test_sentences  = [(s,h[c]) for (s,c) in repr_test_sentences]
    #############################################
    # Training #
    repr_train_sentences,repr_test_sentences = classes_to_nn(repr_train_sentences,repr_test_sentences,n_classes)
    XT,YT = unzip_2d(repr_train_sentences)
    Xt,Yt = unzip_2d(repr_test_sentences)
    net.train_bfgs(XT,YT)
    print "Trained"
    ############
    output,regress = net.test(Xt,Yt)
    for i in xrange(len(Yt)):
	correct_class   = Yt[i].index(1)
	predicted_class = where(output[i]==max(output[i]))[0][0]
	if correct_class!=predicted_class: err += 1
    return 1.0-(float(err)/len(repr_test_sentences))
    
#################################### End Partition ####################################

#################################### Resustitution ####################################

def resustitution_nearest(sentences,k=1):
	if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 
	model = Word2Vec([sentence for (sentence,cat) in sentences],min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V)
	act_index,act_test_sample,err = 0,None,0
	train_sentences_rep = get_sentences_representation_supervised(sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
	while act_index<len(sentences):
		test_sentence = train_sentences_rep[act_index]
		if Config.REGULARIZATION: pass # Implement regularization #
		if Config.VERBOSE: logging.info("Testing test sentence: "+str(act_index))
		c_class = get_class(test_sentence[0],train_sentences_rep,k)
		if Config.VERBOSE: logging.info("Sample class: "+str(c_class)+" and correct class is: "+test_sentence[1])
		if c_class!=test_sentence[1]: err += 1
		act_index += 1
	print "Accuracy = ",1.0-(float(err)/len(sentences))

def resustitution_supervised_classifier(sentences,clf):
	if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 
	model = Word2Vec([sentence for (sentence,cat) in sentences],min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V)
	act_index,act_test_sample,err = 0,None,0
	train_sentences_rep = get_sentences_representation_supervised(sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
	""" Training """
	h,s,c = {},set([cat for (sentence,cat) in train_sentences_rep]),0
	for cat in s: h[cat] = c; c += 1
	X,Y = [],[]
	for (train_sentence,cat) in train_sentences_rep: 
		X.append(train_sentence); 
		Y.append(h[cat])
	clf.fit(X,Y)
	while act_index<len(sentences):
		print act_index
		test_sentence = train_sentences_rep[act_index]
		if Config.REGULARIZATION: pass # Implement regularization #
		if Config.VERBOSE: logging.info("Testing test sentence: "+str(act_index))
		""" Testing """
		y = clf.predict(test_sentence[0])
		if y[0]!=h[test_sentence[1]]: err += 1
		act_index += 1
	print "Accuracy = ",1.0-(float(err)/len(sentences))

#################################### End Resustitution ####################################

#################################### Leaving one out ####################################

def leaving_one_out_nearest(sentences,k=1,fnormalization=None):
	if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 
	act_index,act_test_sample,err = 0,None,0
	while act_index<len(sentences):
		print act_index
		train_sentences     = [sentences[i] for i in xrange(len(sentences)) if act_index!=i]
		model = Word2Vec([sentence for (sentence,cat) in train_sentences],min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V)
		test_sentence = sentences[act_index]
		test_sentence = (Config.SENTENCE_REPRESENTATION(test_sentence[0],model,Config.SIZE_W2V),test_sentence[1])
		train_sentences_rep = get_sentences_representation_supervised(train_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
		if fnormalization!=None:
		    train_sentences_rep = fnormalization(train_sentences_rep)
		    test_sentence  = fnormalization([test_sentence])[0]
		if Config.VERBOSE: logging.info("Testing test sentence: "+str(act_index)+" against "+str(len(train_sentences))+" sentences")
		c_class = get_class(test_sentence[0],train_sentences_rep,k)
		if Config.VERBOSE: logging.info("Sample class: "+str(c_class)+" and correct class is: "+test_sentence[1])
		if c_class!=test_sentence[1]: err += 1
		act_index += 1
	print "Accuracy = ",1.0-(float(err)/len(sentences))


def leaving_one_out_supervised_classifier(sentences,clf,fnormalization=None,train_lengths=None,test_lengths=None):
	if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 
	act_index,act_test_sample,err = 0,None,0
	while act_index<len(sentences):
		print act_index
		train_sentences     = [sentences[i] for i in xrange(len(sentences)) if act_index!=i]
		model = Word2Vec([sentence for (sentence,cat) in train_sentences],min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V,negative=Config.NEG_W2V)
		test_sentence = sentences[act_index]
		test_sentence = (Config.SENTENCE_REPRESENTATION(test_sentence[0],model,Config.SIZE_W2V),test_sentence[1])
		train_sentences_rep = get_sentences_representation_supervised(train_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
		if fnormalization!=None:
		    train_sentences_rep = fnormalization(train_sentences_rep)
		    test_sentence  = fnormalization([test_sentence])[0]
		if Config.VERBOSE: logging.info("Testing test sentence: "+str(act_index)+" against "+str(len(train_sentences))+" sentences")
		""" Training """
		h,s,c = {},set([cat for (sentence,cat) in train_sentences_rep]),0
		for cat in s: h[cat] = c; c += 1
		X,Y = [],[]
		for (train_sentence,cat) in train_sentences_rep: 
			X.append(train_sentence); 
			Y.append(h[cat])
		clf.fit(X,Y)
		""" Testing """
		y = clf.predict(test_sentence[0])
		if y[0]!=h[test_sentence[1]]: err += 1
		act_index += 1
	#print "Accuracy = ",1.0-(float(err)/len(sentences))
	return 1.0-(float(err)/len(sentences))

def leaving_one_out_supervised_classifier_other_model(sentences,clf,fnormalization=None,train_lengths=None,test_lengths=None):
	if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 
	act_index,act_test_sample,err = 0,None,0
	while act_index<len(sentences):
		#print act_index
		train_sentences_rep     = [sentences[i] for i in xrange(len(sentences)) if act_index!=i]
		test_sentence = sentences[act_index]
		if fnormalization!=None:
		    train_sentences_rep = fnormalization(train_sentences_rep)
		    test_sentence  = fnormalization([test_sentence])[0]
		if Config.VERBOSE: logging.info("Testing test sentence: "+str(act_index)+" against "+str(len(train_sentences))+" sentences")
		""" Training """
		h,s,c = {},set([cat for (sentence,cat) in train_sentences_rep]),0
		for cat in s: h[cat] = c; c += 1
		X,Y = [],[]
		for (train_sentence,cat) in train_sentences_rep: 
			X.append(train_sentence); 
			Y.append(h[cat])
		clf.fit(X,Y)
		""" Testing """
		y = clf.predict(test_sentence[0])
		if y[0]!=h[test_sentence[1]]: err += 1
		act_index += 1
	#print "Accuracy = ",1.0-(float(err)/len(sentences))
	return 1.0-(float(err)/len(sentences))
	
#################################### End Leaving one out ####################################
