#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pydasm import get_instruction,get_instruction_string,MODE_32,FORMAT_INTEL
from pefile import PE
from os import walk
from sklearn.preprocessing import normalize
try: from cPickle import dump,load,HIGHEST_PROTOCOL
except: from pickle import dump,load,HIGHEST_PROTOCOL

def serialize(file_name,object): 
	with open(file_name,'wb') as fd: dump(object,fd,HIGHEST_PROTOCOL)
	
def unserialize(file_name):
	res = []
	with open(file_name,'rb') as fd: res.append(load(fd))
	return res

def get_terms_from_lexicon(pos_file,neg_file):
    POS,NEG = [],[]
    with open(pos_file) as fd_pos:
	for line in fd_pos.readlines(): POS.append(line.strip().decode("utf8","ignore"))
    with open(neg_file) as fd_neg:
	for line in fd_neg.readlines(): NEG.append(line.strip().decode("utf8","ignore"))
    return POS,NEG
    
def get_sentences_supervised_from_folders(path):
	""" Corpus tipo corpus tcstar (temas separados en carpetas y cada fichero un articulo) """
	res = []
	file_names = get_files(path)
	for filename in file_names:
		with open(filename) as fd_file:
			article = fd_file.read()
			if len(article)>1:
				res.append( (article.decode("utf8","ignore"),filename[filename.rfind("\\")+1:filename.rfind("/")]) )
	return res
	
def get_sentences_unsupervised_from_folders(path):
	""" Corpus tipo corpus tcstar (temas separados en carpetas y cada fichero un articulo) """
	res = []
	file_names = get_files(path)
	for filename in file_names:
		with open(filename) as fd_file:
			article = fd_file.read()
			if len(article)>1:
				res.append(article.decode("utf8","ignore"))
	return res

def get_classes_from_file(classes_file):
    """ Extrae el nombre de las clases en orden de indice empezando desde 1 (para corpus tipo dbpedia csv) """
    res = []
    with open(classes_file) as c_file:
	for c in c_file.readlines(): res.append(c.strip())
    return res
    
def get_sentences_supervised_from_files_csv(filename,classes):
    """ Corpus tipo los corpus supervisados de dbpedia csv (cada linea contiene un articulo y su clase) """
    res = []
    with open(filename) as fd_file:
	for line in fd_file.readlines():
	    aux = line.split(",")
	    if len(aux[2])>1: res.append((aux[2][1:-2].strip().decode("utf8","ignore"),classes[int(aux[0])-1]))
    return res

def get_sentences_unsupervised_from_files_csv(filename,classes):
    """ Corpus tipo los corpus supervisados de dbpedia csv (cada linea contiene un articulo y su clase) """
    res = []
    with open(filename) as fd_file:
	for line in fd_file.readlines():
	    aux = line.split(",")
	    if len(aux[2])>1: res.append(aux[2][1:-2].strip().decode("utf8","ignore"))
    return res
    
def get_sentences_supervised_from_files(path):
	""" Corpus tipo los corpus supervisados de prueba (temas separados en ficheros, cada linea contiene un articulo) """
	res = []
	file_names = get_files(path)
	for filename in file_names:
		with open(filename) as fd_file:	
			for line in fd_file.readlines():
				line = line[:-1] 	
				if len(line)>1: 
					res.append( (line.decode("utf8","ignore"),filename[filename.rfind("/")+1:len(filename)-4]) )
		fd_file.close()
	return res

def get_sentences_unsupervised_from_files(path):
	""" Corpus tipo los corpus no supervisados de prueba (temas separados en ficheros, cada linea contiene un articulo) """
	res = []
	file_names = get_files(path)
	for filename in file_names:
		with open(filename) as fd_file:		
			for line in fd_file.readlines():
				line = line[:-1] 	
				if len(line)>1: res.append(line.decode("utf8","ignore"))
		fd_file.close()
	return res

def get_queries_dict(path):
    """ Para el reto de queries, extrae las queries, una por fichero """
    res = {}
    file_names = get_files(path)
    for filename in file_names:
	aux_filename = filename[filename.rfind("/")+1:]
	res[aux_filename] = []
	with open(filename) as fd_file:
	    for line in fd_file.readlines():
		if len(line)>1:
		    query = line.split(" ")
		    res[aux_filename].append(query[-3].decode("utf8","ignore"))
		    res[aux_filename].append(query[-2].decode("utf8","ignore"))
		    res[aux_filename].append(query[-1].decode("utf8","ignore"))
		    res[aux_filename].append(" ".join(query[:-3]).decode("utf8","ignore"))
	fd_file.close()
    return res

def get_exec_instructions_supervised(path,c): 
    """ Para el detector de malware, extrae las instrucciones de cada fichero y las almacena en un vector (matriz de instrucciones) """
    res = []
    file_names = get_files(path)
    for filename in file_names:
	try: pe = PE(filename)
	except: 
	    print "Error loading %s"%filename
	    continue
	entry_point = pe.OPTIONAL_HEADER.AddressOfEntryPoint
	ep_ava = entry_point
	data = pe.get_memory_mapped_image()[entry_point:entry_point+pe.OPTIONAL_HEADER.SizeOfCode]
	offset,instructions = 0,[]
	while offset<len(data):
	    instruction = get_instruction(data[offset:],MODE_32)
	    if instruction==None: break
	    else:
		instructions.append(get_instruction_string(instruction,FORMAT_INTEL,ep_ava+offset))
		offset += instruction.length
	res.append((instructions,c))
    return res

def get_exec_instructions_supervised_without_repeat(path,c): 
    """ Para el detector de malware, extrae las instrucciones de cada fichero y las almacena en un vector (matriz de instrucciones) """
    res = []
    file_names = get_files(path)
    for filename in file_names:
	try: pe = PE(filename)
	except: 
	    print "Error loading %s"%filename
	    continue
	entry_point = pe.OPTIONAL_HEADER.AddressOfEntryPoint
	ep_ava = entry_point
	data = pe.get_memory_mapped_image()[entry_point:entry_point+pe.OPTIONAL_HEADER.SizeOfCode]
	offset,instructions = 0,set()
	while offset<len(data):
	    instruction = get_instruction(data[offset:],MODE_32)
	    if instruction==None: break
	    else:
		instructions.add(get_instruction_string(instruction,FORMAT_INTEL,ep_ava+offset))
		offset += instruction.length
	res.append((list(instructions),c))
    return res
    
def get_exec_instructions_unsupervised_without_repeat(path):
    """ Para el detector de malware, extrae las instrucciones de cada fichero y las almacena en un vector (vector con vectores de instrucciones) """
    res = []
    file_names = get_files(path)
    for filename in file_names:
	try: pe = PE(filename)
	except: 
	    print "Error loading %s"%filename
	    continue
	entry_point = pe.OPTIONAL_HEADER.AddressOfEntryPoint
	ep_ava = entry_point
	data = pe.get_memory_mapped_image()[entry_point:entry_point+pe.OPTIONAL_HEADER.SizeOfCode]
	offset,instructions = 0,set()
	while offset<len(data):
	    instruction = get_instruction(data[offset:],MODE_32)
	    if instruction==None: break
	    else:
		instructions.add(get_instruction_string(instruction,FORMAT_INTEL,ep_ava+offset))
		offset += instruction.length
	res.append(list(instructions))
    return res

def get_exec_instructions_unsupervised(path):
    """ Para el detector de malware, extrae las instrucciones de cada fichero y las almacena en un vector (vector con vectores de instrucciones) """
    res = []
    file_names = get_files(path)
    for filename in file_names:
	try: pe = PE(filename)
	except: 
	    print "Error loading %s"%filename
	    continue
	entry_point = pe.OPTIONAL_HEADER.AddressOfEntryPoint
	ep_ava = entry_point
	data = pe.get_memory_mapped_image()[entry_point:entry_point+pe.OPTIONAL_HEADER.SizeOfCode]
	offset,instructions = 0,[]
	while offset<len(data):
	    instruction = get_instruction(data[offset:],MODE_32)
	    if instruction==None: break
	    else:
		instructions.append(get_instruction_string(instruction,FORMAT_INTEL,ep_ava+offset))
		offset += instruction.length
	res.append(instructions)
    return res
       
def get_queries_mat(path):
    """ Para el reto de queries, extrae las queries, una por fichero (vector con las queries)"""
    res = []
    file_names = get_files(path)
    file_names = sorted(file_names,key=lambda s: int(s.split("_")[-1]))
    for filename in file_names:
	with open(filename) as fd_file:
	    line = fd_file.readline();
	    if len(line)>1:
		query = line.split(" ")
		res.append(" ".join(query[:-3]).decode("utf8","ignore"))
	fd_file.close()
    return res

def get_segments_mat(path):
    """ Para el reto de queries, extrae los segmentos, muchos por fichero (matriz) """
    res = []
    file_names = get_files(path)
    for filename in file_names:
	res.append([])
	with open(filename,"r") as fd_file:
	    for line in fd_file.readlines():
		if len(line)>1:
		    line = line[:-1]
		    segment = line.split(" ")
		    res[-1].append(" ".join(segment[:-2]).decode("utf8","ignore"))
	fd_file.close()
    return res
    
def get_segments_dict(path):
    """ Para el reto de queries, extrae los segmentos, muchos por fichero """
    res = {}
    file_names = get_files(path)
    for filename in file_names:
	with open(filename) as fd_file:
	    i = 0
	    filename = filename[filename.rfind("/")+1:]
	    for line in fd_file.readlines():
		if len(line)>1:
		    line = line[:-1]
		    hashName = filename+"_"+str(i)
		    res[hashName] = []
		    query = line.split(" ")
		    res[hashName].append(query[-2].decode("utf8","ignore"))
		    res[hashName].append(query[-1].decode("utf8","ignore"))
		    res[hashName].append(" ".join(query[:-2]).decode("utf8","ignore"))
		i+=1
	fd_file.close()
    return res

def get_files(path):
	res = []
	for root,dirs,files in walk(path):
		for fd in files: res.append(root+"/"+fd)
	return res

def regularization(sentence,val_max_sentences,val_min_sentences): 
	return (sentence-val_min_sentences)/val_max_sentences

def get_most_repeated_kgram(sentences,k):
	h = {}
	for sentence in sentences:
		for i in xrange(len(sentence)-k+1):
			kgram = " ".join(sentence[i:i+k])
			if kgram not in h: h[kgram] = 1
			else:			   h[kgram] += 1
	return sorted([(key,val) for (key,val) in h.items()],key=lambda x:x[1],reverse=True)[0][0]

def get_kgrams_for_line_supervised(sentences,k):
    res = []
    for (sentence,cat) in sentences:
	res.append(([],cat))
	for i in xrange(len(sentence)-k+1):
	    kgram = "".join(sentence[i:i+k])
	    res[-1][0].append(kgram)
    return res

def normalize_norm_unsupervised_samples(X,norm="l2"):
    return normalize(X,norm=norm)

def normalize_norm_supervised_samples(X,norm="l2"):
    Z,Y = zip(*X)
    Z = normalize(Z,norm=norm)
    return zip(Z,Y)

""" NUMPY MATRIX :( """
def normalize_length_supervised_samples(X,lengths): 
    Z,Y = zip(*X)
    Z   = list(Z)
    for i in xrange(len(Z)): 
	Z[i] /= max(lengths[i],1)
    return zip(Z,Y)

def normalize_length_unsupervised_samples(X,lengths):
    for i in xrange(len(X)): X[i] /= max(lengths[i],1)
    return X
    
def unzip_2d(samples):
    X,Y = [],[]
    for i in xrange(len(samples)): 
	X.append(samples[i][0])
	Y.append(samples[i][1])
    return X,Y

def zip_2d(X,Y): return zip(X,Y)

def classes_to_nn(train_sentences,test_sentences,n_classes):
    train_sentences = [(s,[0 if i!=c else 1 for i in xrange(n_classes)]) for (s,c) in train_sentences]
    test_sentences  = [(s,[0 if i!=c else 1 for i in xrange(n_classes)]) for (s,c) in test_sentences]
    return train_sentences,test_sentences
    
def vector_to_unicode(v):
    for i in xrange(len(v)): v[i] = v[i].decode("utf8","ignore")
    return v

def sentences_from_tokenized(X): return [" ".join(X[i]) for i in xrange(len(X))]

def get_most_tfidf_word(sentences): pass

def plus(a,b): return a+b
def sub(a,b): return a-b
def mult(a,b): return a*b
def div(a,b):  return float(a)/b
def eq(a,b): return b
