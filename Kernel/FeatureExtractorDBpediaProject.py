#!/usr/bin/env python
# -*- coding: utf-8 -*-

from numpy import cov,array,zeros,ones
import Config

def get_sentence_representation_sum(s,mode,type_search):
    res = zeros(len(type_search))
    for term in s:
	term_vector = zeros(len(type_search))
	try:
	    Config.SPARQL_ENDPOINT.setQuery("PREFIX db: "+Config.SPARQL_PREFIX+" SELECT ?onto ?value WHERE { db:"""+term+" ?onto ?value  . }")
	    results = Config.SPARQL_ENDPOINT.query().convert()
	    for result in results["results"]["bindings"]: 
		aux = result["onto"]["value"]
		aux = aux[aux.rfind("/")+1:]
		if aux in type_search: 
		    if mode==0:   term_vector[type_search[aux]]  = 1
		    elif mode==1: term_vector[type_search[aux]] += 1
	except Exception as e: print "Term error:" ,term
	res += term_vector
    if Config.VERBOSE: print "Finished sentence."
    return res
    

def get_sentence_representation_mult(s,mode,type_search): 
    res = zeros(len(type_search))
    for term in s:
	term_vector = zeros(len(type_search))
	try:
	    Config.SPARQL_ENDPOINT.setQuery("PREFIX db: "+Config.SPARQL_PREFIX+" SELECT ?onto ?value WHERE { db:"""+term+" ?onto ?value  . }")
	    results = Config.SPARQL_ENDPOINT.query().convert()
	    for result in results["results"]["bindings"]: 
		aux = result["onto"]["value"]
		aux = aux[aux.rfind("/")+1:]
		if aux in type_search: 
		    if mode==0:   term_vector[type_search[aux]]  = 1
		    elif mode==1: term_vector[type_search[aux]] += 1
	except: pass
	res *= term_vector
    return res

def get_sentence_representation_distance(s,mode,type_search):
    res = zeros(len(type_search))
    for term in s:
	term_vector = zeros(len(type_search))
	try:
	    Config.SPARQL_ENDPOINT.setQuery("PREFIX db: "+Config.SPARQL_PREFIX+" SELECT ?onto ?value WHERE { db:"""+term+" ?onto ?value  . }")
	    results = Config.SPARQL_ENDPOINT.query().convert()
	    for result in results["results"]["bindings"]: 
		aux = result["onto"]["value"]
		aux = aux[aux.rfind("/")+1:]
		if aux in type_search: 
		    if mode==0:   term_vector[type_search[aux]]  = 1
		    elif mode==1: term_vector[type_search[aux]] += 1
	except: pass
	res += Config.DISTANCE_REPRESENTATION(res,term_vector)
    return res

def get_sentence_representation_weighted(s,weights,mode,type_search):
    res = zeros(len(type_search))
    for term in s:
	term_vector = zeros(len(type_search))
	try:
	    Config.SPARQL_ENDPOINT.setQuery("PREFIX db: "+Config.SPARQL_PREFIX+" SELECT ?onto ?value WHERE { db:"""+term+" ?onto ?value  . }")
	    results = Config.SPARQL_ENDPOINT.query().convert()
	    for result in results["results"]["bindings"]: 
		aux = result["onto"]["value"]
		aux = aux[aux.rfind("/")+1:]
		if aux in type_search: 
		    if mode==0:   term_vector[type_search[aux]]  = 1
		    elif mode==1: term_vector[type_search[aux]] += 1
	except: pass
	res += weights*term_vector
    return res

def get_sentence_representation_covariance(s,mode,type_search):
    res = zeros(len(type_search))
    for term in s:
	term_vector = zeros(len(type_search))
	try:
	    Config.SPARQL_ENDPOINT.setQuery("PREFIX db: "+Config.SPARQL_PREFIX+" SELECT ?onto ?value WHERE { db:"""+term+" ?onto ?value  . }")
	    results = Config.SPARQL_ENDPOINT.query().convert()
	    for result in results["results"]["bindings"]: 
		aux = result["onto"]["value"]
		aux = aux[aux.rfind("/")+1:]
		if aux in type_search: 
		    if mode==0:   term_vector[type_search[aux]]  = 1
		    elif mode==1: term_vector[type_search[aux]] += 1
	except: pass
	res += term_vector
    return cov(res)/len(s)

def get_sentences_representation_supervised(sentences,frepresentation,type_search,mode=0,weights=None): 
    if weights==None: return [(frepresentation(sentence,mode,type_search),cat) for (sentence,cat) in sentences]
    else: return [(frepresentation(sentence,mode,weights,type_search),cat) for (sentence,cat) in sentences]

def get_sentences_representation_unsupervised(sentences,frepresentation,type_search,mode=0,weights=None): 
    if weights==None: return [frepresentation(sentence,mode,type_search) for sentence in sentences]
    else: [frepresentation(sentence,mode,weights,type_search) for sentence in sentences]
