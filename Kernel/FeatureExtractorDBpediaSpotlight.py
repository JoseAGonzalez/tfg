#!/usr/bin/env python
# -*- coding: utf-8 -*-

from numpy import cov,array,zeros,ones
import urllib2
import urllib
import json
import Config

def get_sentence_representation_sum(s,mode,type_search,confidence):
    res = zeros(len(type_search))
    try:
	data = urllib.urlencode({"text":s,"confidence":confidence})
    except: return res
    req = urllib2.Request(Config.SPOTLIGHT_ENDPOINT,data)
    req.add_header("Accept",Config.SPOTLIGHT_APPLICATION)
    req.add_header("Application","application/x-www-form-urlencoded")
    f = urllib2.urlopen(req)
    entities = json.load(f)
    if "Resources" not in entities: return res
    entities = entities["Resources"]
    for term in entities:
	term_vector = zeros(len(type_search))
	uri = term["@URI"]
	term = uri[uri.rfind("/")+1:]
	try:
	    Config.SPOTLIGHT_SPARQL_ENDPOINT.setQuery("SELECT ?onto ?value WHERE { <"+Config.SPOTLIGHT_SPARQL_PREFIX+term+"> ?onto ?value  . }")
	    results = Config.SPOTLIGHT_SPARQL_ENDPOINT.query().convert()
	    for result in results["results"]["bindings"]: 
		aux = result["onto"]["value"]
		aux = aux[aux.rfind("/")+1:]
		if aux in type_search: 
		    if mode==0:   term_vector[type_search[aux]]  = 1
		    elif mode==1: term_vector[type_search[aux]] += 1
	except: continue
	res += term_vector
    if Config.VERBOSE: print "Finished sentence."
    return res

def get_sentences_representation_supervised(sentences,frepresentation,type_search,mode=0,weights=None,confidence=0.35): 
    if weights==None: return [(frepresentation(sentence,mode,type_search,confidence),cat) for (sentence,cat) in sentences]
    else: return [(frepresentation(sentence,mode,weights,type_search,confidence),cat) for (sentence,cat) in sentences]
    
    
def get_sentences_representation_unsupervised(sentences,frepresentation,type_search,mode=0,weights=None,confidence=0.35): 
    if weights==None: return [frepresentation(sentence,mode,type_search,confidence) for sentence in sentences]
    else: [frepresentation(sentence,mode,weights,type_search,confidence) for sentence in sentences]
