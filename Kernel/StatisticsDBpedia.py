#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import Config
from NeighboursClassifier import get_class
from FeatureExtractorDBpediaProject import get_sentences_representation_supervised,get_sentences_representation_unsupervised
from warnings import filterwarnings

# Filter warnings from other libs #
filterwarnings("ignore")
###################################

#################################### Partition ####################################

def partition_nearest(repr_train_sentences,repr_test_sentences,k=1,mode=0,weights=None):
	if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 
	err,i = 0,0
	for repr_test_sentence in repr_test_sentences:
	    print i
	    if Config.VERBOSE: logging.info("Testing test sentence: "+str(i))
	    c_class = get_class(repr_test_sentence[0],repr_train_sentences,k)
	    if Config.VERBOSE: logging.info("Sample class: "+str(c_class)+" and correct class is: "+repr_test_sentence[1])
	    if c_class!=repr_test_sentence[1]: err += 1
	    i += 1
	print "Accuracy: ",1.0-(float(err)/len(repr_test_sentences))


def partition_supervised(repr_train_sentences,repr_test_sentences,clf,mode=None):
	if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 
	err,i = 0,0
	""" Training """
	h,s,c = {},set([cat for (sentence,cat) in repr_train_sentences]),0
	for cat in s: h[cat] = c; c += 1
	X,Y = [],[]
	for (repr_train_sentence,cat) in repr_train_sentences: 
		X.append(repr_train_sentence); 
		Y.append(h[cat])
	clf.fit(X,Y)
	for repr_test_sentence in repr_test_sentences:
		if Config.VERBOSE: logging.info("Testing test sentence: "+str(i))
		""" Testing """
		y = clf.predict(repr_test_sentence[0]);
		if y[0]!=h[repr_test_sentence[1]]: err += 1
		if Config.VERBOSE: logging.info("Sample class: "+str(y[0])+" and correct class is: "+repr_test_sentence[1])
		i += 1
	#print "Accuracy: ",1.0-(float(err)/len(repr_test_sentences))
	return 1.0-(float(err)/len(repr_test_sentences))
#################################### End Partition ####################################

#################################### Resustitution ####################################

def resustitution_nearest(repr_sentences,k=1,mode=0,weights=None):
	if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 
	err,i = 0,0
	for repr_sentence in repr_sentences:
	    if Config.VERBOSE: logging.info("Testing test sentence: "+str(i))
	    c_class = get_class(repr_sentence[0],repr_sentences,k)
	    if Config.VERBOSE: logging.info("Sample class: "+str(c_class)+" and correct class is: "+repr_sentence[1])
	    if c_class!=repr_sentence[1]: err += 1
	    i += 1
	print "Accuracy: ",1.0-(float(err)/len(repr_sentences))


def resustitution_supervised(repr_sentences,clf,mode=None):
	if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 
	err,i = 0,0
	""" Training """
	h,s,c = {},set([cat for (sentence,cat) in repr_sentences]),0
	for cat in s: h[cat] = c; c += 1
	X,Y = [],[]
	for (repr_sentence,cat) in repr_sentences: 
		X.append(repr_sentence); 
		Y.append(h[cat])
	clf.fit(X,Y)
	for repr_sentence in repr_sentences:
		if Config.VERBOSE: logging.info("Testing test sentence: "+str(i))
		""" Testing """
		y = clf.predict(repr_sentence[0]);
		#print i
		if y[0]!=h[repr_sentence[1]]: err += 1
		if Config.VERBOSE: logging.info("Sample class: "+str(y[0])+" and correct class is: "+repr_sentence[1])
		i += 1
	#print "Accuracy: ",1.0-(float(err)/len(repr_sentences))
	return 1.0-(float(err)/len(repr_sentences))
#################################### End Resustitution ####################################


#################################### Leaving one out ####################################

#################################### End Leaving one out ####################################
