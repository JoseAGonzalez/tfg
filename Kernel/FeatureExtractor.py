#!/usr/bin/env python
# -*- coding: utf-8 -*-

from numpy import cov,array,zeros,ones
from sklearn.feature_extraction.text import TfidfVectorizer
import Config

"""def get_bow_from_sentences(X,min_df=1):
    vectorizer  = CountVectorizer(min_df=1)
    return vectorizer.fit_transform(X)
    
def get_tf_idf_from_bow(bow):
    transformer = TfidfTransformer()
    return transformer.fit_transform(bow)
"""

def get_tf_idf_dict(tfidf_model):
    idf = tfidf_model.idf_
    return dict(zip(tfidf_model.get_feature_names(),idf))

def get_tf_idf_corpus(corpus):
    vectorizer = TfidfVectorizer(min_df=1)
    return vectorizer,vectorizer.fit_transform(corpus).toarray()
    
def get_sentence_representation_w2v_tfidf(s,model,w2v_size,d_weights):
    res = zeros(w2v_size)
    for i in xrange(1,len(s)):
	try: res += (model[s[i]]*d_weights[s[i]])
	except: continue
    return res

## Sacar el centroide, calculando el punto medio y viendo cual es la palabra que más se le acerca ##
def get_sentence_representation_centroid_word(s,model,w2v_size):
    v=[]
    a = zeros(w2v_size)
    for i in xrange(len(s)):
	try: 
	    v.append(model[s[i]])
	    a += model[s[i]]
	except: continue
    a /= len(v)
    m,mi = float("inf"),-1
    for i in xrange(len(v)):
	d = Config.DISTANCE_REPRESENTATION(v[i],a)
	if d<m: m,mi = d,i
    if mi!=-1: return v[mi]
    else:      return zeros(w2v_size)

def get_sentence_representation_centroid(s,model,w2v_size):
    a = zeros(w2v_size)
    for i in xrange(len(s)):
	try: a += model[s[i]]
	except: continue
    if len(s)>0: a /= len(s)
    return a
    
def get_sentence_representation_sum(s,model,w2v_size): # VECTOR = SUMA(VECTOR ANTERIOR,VECTOR SIG PALABRA)
	try:     res = zeros(w2v_size)+model[s[0]]
	except:  res = zeros(w2v_size)
	for i in xrange(1,len(s)):
		try: 	res += model[s[i]]
		except: continue
	return res

def get_sentence_representation_mult(s,model,w2v_size): # VECTOR = PRODUCTO(VECTOR ANTERIOR,VECTOR SIG PALABRA)
	try:     res = ones(w2v_size)*model[s[0]]
	except:  res = ones(w2v_size)
	for i in xrange(1,len(s)):
		try: 	res *= model[s[i]]
		except: continue
	return res

def get_sentence_representation_derivative(s,model,w2v_size): # VECTOR = DIFF DE SEMÁNTICAS ENTRE LA PALABRA Wi Y EL CONJUNTO DE PALABRAS Wj 0<j<i #
	res = zeros(w2v_size)
	for i in xrange(0,len(s)-1):
	    try: res += model[s[i+1]]-model[s[i]]
	    except: continue
	return res

def get_sentence_representation_weighted(s,model,w2v_size): # SUMA PONDERADA POR LA POSICIÓN DE CADA PALABRA (generalización darle un peso diferente a cada palabra) #
	res = zeros(w2v_size)
	for i in xrange(len(s)):
		try: 	res += (1+i)*model[s[i]]
		except: continue
	return res

def get_sentence_representation_covariance(s,model): # VECTOR = SUMA NORMALIZADA DE LAS FILAS DE LA MATRIZ DE COVARIANZA DE M=MATRIZ FORMADA POR LOS VECTORES DE PALABRAS QUE LO COMPONEN #
	try:     res = [model[s[0]]]
	except:  res = []
	for i in xrange(1,len(s)):
		try: 	res.append(model[s[i]])
		except: res = res
	return cov(array(res))/len(s)

	
def get_sentences_representation_supervised(sentences,model,frepresentation,w2vs,weights=None): 
    if weights==None: return [(frepresentation(sentence,model,w2v_size=w2vs),cat) for (sentence,cat) in sentences]
    else: return [(frepresentation(sentence,model,w2v_size=w2vs,d_weights=weights),cat) for (sentence,cat) in sentences]

def get_sentences_representation_unsupervised(sentences,model,frepresentation,w2vs,weights=None): 
    if weights==None: return [frepresentation(sentence,model,w2v_size=w2vs) for sentence in sentences]
    else: return [frepresentation(sentence,model,w2v_size=w2vs,d_weights=weights) for sentence in sentences]

def get_queries_dict_representation(queries,model,frepresentation,w2vs,weights=None):
    if weights==None:
	for key in queries: queries[key][3] = frepresentation(queries[key][3],model,w2v_size=w2vs)
    else:
	for key in queries: queries[key][3] = frepresentation(queries[key][3],model,weights,w2v_size=w2vs)
    return queries
    
def get_segments_dict_representation(segments,model,frepresentation,w2vs,weights=None):
    if weights==None:
	for key in segments: segments[key][2] = frepresentation(segments[key][2],model,w2v_size=w2vs)
    else:
	for key in segments: segments[key][2] = frepresentation(segments[key][2],model,weights,w2v_size=w2vs)
    return segments
    
    
def get_queries_mat_representation(queries,model,frepresentation,w2vs,weights=None):
    if weights==None:
	for i in xrange(len(queries)): queries[i] = frepresentation(queries[i],model,w2v_size=w2vs)
    else:
	for i in xrange(len(queries)): queries[i] = frepresentation(queries[i],model,weights,w2v_size=w2vs)
    return queries
    
def get_segments_mat_representation(segments,model,frepresentation,w2vs,weights=None):
    if weights==None:
	for i in xrange(len(segments)):
	    for j in xrange(len(segments[i])):
		segments[i][j] = frepresentation(segments[i][j],model,w2v_size=w2vs)
    else:
	for i in xrange(len(segments)):
	    for j in xrange(len(segments[i])):
		segments[i][j] = frepresentation(segments[i][j],model,weights,w2v_size=w2vs)
    return segments
