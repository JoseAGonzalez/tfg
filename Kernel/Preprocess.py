#!/usr/bin/env python
# -*- coding: utf-8 -*-

import Config
import logging
import re
from re import sub

TOKENIZER_RE = re.compile(r"[A-Z]{2,}(?![a-z])|[A-Z][a-z]+(?=[A-Z])|[\'\w\-]+",re.UNICODE)

def delete_duplicates(s):      
	sn = ""
	for i in xrange(len(s)):
		if i==0: sn += s[i]
		else:    sn += s[i] if s[i]!=sn[-1] else ""
	return sn

def remove_non_alfanumeric_1(sentence): return sub(u"[^\wáéíóúñçàèìòù]",u" ",sentence)

def remove_non_alfanumeric_2(sentence,non_alfanumeric):
    for non_alfa in non_alfanumeric:
	sentence = sentence.replace(non_alfa,u" ")
    return sentence

def skflow_tokenizer_supervised(sentences):
    res = []
    for (sentence,cat) in sentences: res.append((TOKENIZER_RE.findall(sentence),cat))
    return res

def skflow_tokenizer_unsupervised(sentences):
    res = []
    for sentence in sentences: res.append(TOKENIZER_RE.findall(sentence))
    return res

def tokenizer(sentence,pattern_tokenize,lowercase): 
	if lowercase==True: return [word.strip().lower() for word in sentence.split(pattern_tokenize) if word!=""]
	else: return [word.strip() for word in sentence.split(pattern_tokenize) if word]
    
def remove_stopwords(tokens,stopwords):     return [word for word in tokens if word not in stopwords if word!=""]

def stemming(tokens,stemmer):     return [stemmer.stem(word) for word in tokens]

def lemmatize(tokens,lemmatizer):   return [lemmatizer.lemmatize(word,pos='v') for word in tokens]

def split_sentence(s): return s.split(" ")

def get_ngrams(tokens,n): return list(set([" ".join(tokens[i:i+n]) for i in xrange(len(tokens)-n+1)]))

def prepare_dbpedia(sentence):
    new_sentence,i = [],0
    while i < len(sentence)-1:
	if sentence[i][0].isupper() and sentence[i+1][0].isupper(): new_sentence.append(sentence[i]+"_"+sentence[i+1]);i += 2
	else: new_sentence.append(sentence[i]); i += 1
    if len(sentence)>1 and sentence[-1] not in new_sentence[-1]: new_sentence.append(sentence[-1]) 
    return new_sentence
	
def normalize(sentence,lowercase=True,make_stemming=False,make_lemmatize=False,remove_duplicates=True,remove_stopw=True,remove_alfanumeric=True,ngrams=1,dbpedia=False,non_alfanumeric_func=Config.NON_ALFANUMERIC_FUNC,stemmer=Config.STEMMER,lemmatizer=Config.LEMMATIZER,pattern_tokenize=Config.PATTERN_TOKENIZER,stopwords=Config.STOPWORDS,non_alfanumeric_patterns=Config.NON_ALFANUMERIC_PATTERNS): 
	# Remove alfanumeric #
	if remove_alfanumeric==True: 
	    if non_alfanumeric_func==1: sentence = remove_non_alfanumeric_1(sentence)
	    if non_alfanumeric_func==2: sentence = remove_non_alfanumeric_2(sentence,non_alfanumeric_patterns)
	    
	# Tokenize #
	if dbpedia==False: tokens = tokenizer(sentence,pattern_tokenize,lowercase)
	else: tokens = prepare_dbpedia(tokenizer(sentence,pattern_tokenize,False))
	
	# Remove duplicate letters #
	if remove_duplicates==True:  tokens = [delete_duplicates(token) for token in tokens]
	# Stopwords #
	if remove_stopw==True:      tokens = remove_stopwords(tokens,stopwords)
	# Lemmatize #
	if make_lemmatize==True:    tokens = lemmatize(tokens,lemmatizer)
	# Stemming  #
	if make_stemming==True:     tokens = stemming(tokens,stemmer)
	# Get ngrams #
	if ngrams!=1: 		  tokens = get_ngrams(tokens,ngrams)
	# DBPedia capitalize #
	if dbpedia==True: tokens = [token.capitalize() for token in tokens]
	return tokens
    
def normalize_sentences_supervised(sentences,lowercase=True,make_stemming=False,make_lemmatize=False,remove_duplicates=True,remove_stopw=True,remove_alfanumeric=True,ngrams=1,dbpedia=False,non_alfanumeric_func=Config.NON_ALFANUMERIC_FUNC,stemmer=Config.STEMMER,lemmatizer=Config.LEMMATIZER,pattern_tokenize=Config.PATTERN_TOKENIZER,stopwords=Config.STOPWORDS,non_alfanumeric_patterns=Config.NON_ALFANUMERIC_PATTERNS): 
	res = []
	for (sentence,cat) in sentences:
	    res.append((normalize(sentence,lowercase,make_stemming,make_lemmatize,remove_duplicates,remove_stopw,remove_alfanumeric,ngrams,dbpedia,non_alfanumeric_func,stemmer,lemmatizer,pattern_tokenize,stopwords,non_alfanumeric_patterns),cat))
	return res

def normalize_sentences_unsupervised(sentences,lowercase=True,make_stemming=False,make_lemmatize=False,remove_duplicates=True,remove_stopw=True,remove_alfanumeric=True,ngrams=1,dbpedia=False,non_alfanumeric_func=Config.NON_ALFANUMERIC_FUNC,stemmer=Config.STEMMER,lemmatizer=Config.LEMMATIZER,pattern_tokenize=Config.PATTERN_TOKENIZER,stopwords=Config.STOPWORDS,non_alfanumeric_patterns=Config.NON_ALFANUMERIC_PATTERNS): 
	res = []
	for sentence in sentences:
	    res.append(normalize(sentence,lowercase,make_stemming,make_lemmatize,remove_duplicates,remove_stopw,remove_alfanumeric,ngrams,dbpedia,non_alfanumeric_func,stemmer,lemmatizer,pattern_tokenize,stopwords,non_alfanumeric_patterns))
	return res

def normalize_queries_dict(queries,lowercase=True,make_stemming=False,make_lemmatize=False,remove_duplicates=True,remove_stopw=True,remove_alfanumeric=True,ngrams=1,dbpedia=False,non_alfanumeric_func=Config.NON_ALFANUMERIC_FUNC,stemmer=Config.STEMMER,lemmatizer=Config.LEMMATIZER,pattern_tokenize=Config.PATTERN_TOKENIZER,stopwords=Config.STOPWORDS,non_alfanumeric_patterns=Config.NON_ALFANUMERIC_PATTERNS):
    for key in queries:
	queries[key][3] = normalize(queries[key][3],lowercase,make_stemming,make_lemmatize,remove_duplicates,remove_stopw,remove_alfanumeric,ngrams,dbpedia,non_alfanumeric_func,stemmer,lemmatizer,pattern_tokenize,stopwords,non_alfanumeric_patterns)
    return queries
    
def normalize_segments_dict(segments,lowercase=True,make_stemming=False,make_lemmatize=False,remove_duplicates=True,remove_stopw=True,remove_alfanumeric=True,ngrams=1,dbpedia=False,non_alfanumeric_func=Config.NON_ALFANUMERIC_FUNC,stemmer=Config.STEMMER,lemmatizer=Config.LEMMATIZER,pattern_tokenize=Config.PATTERN_TOKENIZER,stopwords=Config.STOPWORDS,non_alfanumeric_patterns=Config.NON_ALFANUMERIC_PATTERNS):
    for key in segments:
	segments[key][2] = normalize(segments[key][2],lowercase,make_stemming,make_lemmatize,remove_duplicates,remove_stopw,remove_alfanumeric,ngrams,dbpedia,non_alfanumeric_func,stemmer,lemmatizer,pattern_tokenize,stopwords,non_alfanumeric_patterns)
    return segments

def normalize_queries_mat(queries,lowercase=True,make_stemming=False,make_lemmatize=False,remove_duplicates=True,remove_stopw=True,remove_alfanumeric=True,ngrams=1,dbpedia=False,non_alfanumeric_func=Config.NON_ALFANUMERIC_FUNC,stemmer=Config.STEMMER,lemmatizer=Config.LEMMATIZER,pattern_tokenize=Config.PATTERN_TOKENIZER,stopwords=Config.STOPWORDS,non_alfanumeric_patterns=Config.NON_ALFANUMERIC_PATTERNS):
    for i in xrange(len(queries)):
	queries[i] = normalize(queries[i],lowercase,make_stemming,make_lemmatize,remove_duplicates,remove_stopw,remove_alfanumeric,ngrams,dbpedia,non_alfanumeric_func,stemmer,lemmatizer,pattern_tokenize,stopwords,non_alfanumeric_patterns)
    return queries
    
def normalize_segments_mat(segments,lowercase=True,make_stemming=False,make_lemmatize=False,remove_duplicates=True,remove_stopw=True,remove_alfanumeric=True,ngrams=1,dbpedia=False,non_alfanumeric_func=Config.NON_ALFANUMERIC_FUNC,stemmer=Config.STEMMER,lemmatizer=Config.LEMMATIZER,pattern_tokenize=Config.PATTERN_TOKENIZER,stopwords=Config.STOPWORDS,non_alfanumeric_patterns=Config.NON_ALFANUMERIC_PATTERNS):
    for i in xrange(len(segments)):
	for j in xrange(len(segments[i])):
	    segments[i][j] = normalize(segments[i][j],lowercase,make_stemming,make_lemmatize,remove_duplicates,remove_stopw,remove_alfanumeric,ngrams,dbpedia,non_alfanumeric_func,stemmer,lemmatizer,pattern_tokenize,stopwords,non_alfanumeric_patterns)
    return segments
