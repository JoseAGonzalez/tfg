#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from Utils import unzip_2d
from Config import COLOURS

""" X = [[[sample_1_topic_1],[sample_2_topic_1],...,[sample_n_topic_1]
     [[sample_1_topic_2],[sample_2_topic_2],...,[sample_m_topic_2],
     ...
     [[sample_1_topic_1],[sample_2_topic_1],...,[sample_k_topic_p]
    ]
    
    Y = [topic_1,topic_2,...,topic_p]
"""

def zip_for_graphic(samples):
    h,X,Y = {},[],[]
    for (s,c) in samples:
	if c not in h: h[c] = [s]
	else:          h[c].append(s)
    for key in h.keys(): 
	for c in h[key]: X.append(c)
	Y.append(key)
    return X,Y
	
def plot_supervised(X,Y):
    pca = PCA(n_components=2)
    pca.fit(X)
    Z = pca.transform(X)
    X1,X2 = unzip_2d(Z)
    h   = {}
    b   = {}
    sY  = list(set(Y))
    for c in xrange(len(sY)):     
	h[sY[c]]   = COLOURS.pop(0)
	b[sY[c]]   = True
    for i in xrange(len(X1)):     
	if b[Y[i]]==True: 
	    plt.plot(X1[i],X2[i],h[Y[i]],label=Y[i])
	    b[Y[i]] = False
	else: plt.plot(X1[i],X2[i],h[Y[i]])
    plt.legend();
    plt.show()

def plot_unsupervised(X):
    pca = PCA(n_components=2)
    pca.fit(X)
    Z = pca.transform(X)
    X1,X2 = unzip_2d(Z)
    plt.plot(X1,X2,'ro')
    plt.show()

if __name__ == "__main__":
    samples_test = [([0.5,0.3],"social"),([0.4,0.7],"social"),([0.4,0.3],"sport"),([0.9,0.2],"sport"),([0.1,0.15],"politics"),([0.2,0.36],"politics")]
    X,Y = zip_for_graphic(samples_test)
    plot_supervised(X,Y)
