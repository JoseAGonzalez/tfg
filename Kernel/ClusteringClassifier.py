#!/usr/bin/env python
# -*- coding: utf-8 -*-
##### MODULAR #####
import sys
sys.path.insert(0, '../')
from sklearn.cluster import KMeans
from Kernel import Utils
from Kernel import Preprocess
from Kernel import FeatureExtractor
from Kernel import Config
from gensim.models import Word2Vec

def get_classes(clustering_classifier,sentences,repr_sentences,kgram):
	""" Clase -> k-grama más frecuente en el conjunto de frases de un mismo cluster """
	clustering_classifier.fit(repr_sentences)
	clusters_samples  = [[] for i in xrange(Config.N_CLUSTERS)]
	clusters_classes  = [None for i in xrange(Config.N_CLUSTERS)]
	samples_labels    = clustering_classifier.labels_
	for i in xrange(len(sentences)): 
	    clusters_samples[samples_labels[i]].append(sentences[i])
	for i in xrange(Config.N_CLUSTERS): 	    
	    if len(clusters_samples[i])>0: clusters_classes[i] = Utils.get_most_repeated_kgram(clusters_samples[i],kgram)
	    else: clusters_classes[i] = "undefined"
	return clusters_classes

def get_learnt_classes(clustering_classifier,repr_sentences):
    """ Clase -> índice de cluster al que pertenece un sample """
    clustering_classifier.fit(repr_sentences)
    return clustering_classifier.labels_
    
def get_class(clustering_classifier,repr_sentence,classes):
	c = clustering_classifier.predict(repr_sentence)
	return classes[c]
