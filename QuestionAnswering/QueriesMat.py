#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.insert(0, '../')
from Kernel import Config
from Kernel import Preprocess
from Kernel import NeighboursClassifier
from Kernel import ClusteringClassifier
from Kernel import FeatureExtractor
from Kernel import Optimizer
from Kernel import Statistics
from Kernel import Utils
from Kernel import Messages
from gensim.models import Word2Vec
import logging

if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 

QUERIES_PATH = "./querys_training_human"
SEGMENT_PATH = "./training_human"

## Información temporal: ##
## Información temporal de queries como una matriz [[query1_id,query1_start,query2_end],...,[queryN_id,queryN_start,queryN_end]] ##
## Información temporal de segmentos como una matriz de matrices [[[segmento_11_start,segmento_11_end],...,[segmento_1N_start,segmento_1N_end]],...,[[segmento_M1_start,segmento_M1_end],...,[segmento_MP_start,segmento_MP_end]]] ##

## Información textual: ##
## Queries como un vector [query_1,query_2,...,query_N] ##
## Segmentos como una matriz [[segmento_11,segmento_12,...,segmento_1N],...,[segmento_M1,segmento_M2,...,segmento_MP]]##
## Obtener las queries y los segmentos, preprocesarlos, extraer caracteristicas partiendo del modelo W2V de la wiki y almacenarlos ##
queries  = Utils.get_queries_mat(QUERIES_PATH)
segments = Utils.get_segments_mat(SEGMENT_PATH)
queries  = Preprocess.normalize_queries_mat(queries,remove_duplicates=False,remove_alfanumeric=True,remove_stopw=False,stopwords=Config.STOPWORDS_FILE)
segments = Preprocess.normalize_segments_mat(segments,remove_duplicates=False,remove_alfanumeric=True,remove_stopw=False,stopwords=Config.STOPWORDS_FILE)
print "Cargando modelo w2v"
#w2vmodel = Word2Vec.load("model_google_news.bin")
w2vmodel = Word2Vec.load_word2vec_format('model_google_news.bin', binary=True)
print segments[0][12]
queries  = FeatureExtractor.get_queries_mat_representation(queries,w2vmodel,Config.SENTENCE_REPRESENTATION,300)
segments = FeatureExtractor.get_segments_mat_representation(segments,w2vmodel,Config.SENTENCE_REPRESENTATION,300)
Utils.serialize("segments_mat_without_punctuation_google.dump",segments)
Utils.serialize("queries_mat_without_punctuation_google.dump",queries)

## Calcular similitudes entre segmentos y queries y almacenarlas ##
queries   = Utils.unserialize("queries_mat_without_punctuation_google.dump")[0]
segments  = Utils.unserialize("segments_mat_without_punctuation_google.dump")[0]
distances = []
for i in xrange(len(segments)):
    distances.append([])
    for j in xrange(len(segments[i])):
	distances[-1].append([])
	for k in xrange(len(queries)):
	    distances[-1][-1].append(NeighboursClassifier.distance_representations(segments[i][j],queries[k]))
Utils.serialize("distances_mat_without_punctuation_google.dump",distances)
## Cargar distancias y almacenar ficheros en formato (2) ##
distances = Utils.unserialize("distances_mat_without_punctuation_google.dump")[0]
#print distances[7][10][420],distances[7][10][960]
#print distances[7][10].index(min(distances[7][10]))+1
#print distances[0][0].index(min(distances[0][0]))+1
for i in xrange(len(distances)):
    with open("./ResultsWithoutPunctuation/res_"+str(i).zfill(3)+".txt","w") as fd:
	for j in xrange(len(distances[i])):
	    fd.write("\t".join(["{:.16f}".format(dist) for dist in distances[i][j]]))
	    fd.write("\n")
    fd.close()
