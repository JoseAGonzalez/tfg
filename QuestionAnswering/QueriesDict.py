#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.insert(0, '../')
from Kernel import Config
from Kernel import Preprocess
from Kernel import NeighboursClassifier
from Kernel import ClusteringClassifier
from Kernel import FeatureExtractor
from Kernel import Optimizer
from Kernel import Statistics
from Kernel import Utils
from Kernel import Messages
from gensim.models import Word2Vec
import logging

if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 

QUERIES_PATH = "./querys_training_human"
SEGMENT_PATH = "./training_human"

## Queries como un diccionario {NOMBRE_FICHERO:[id,tiempo_inicio,tiempo_final,query]} ##
## Segmentos como un diccionario {NOMBRE_FICHERO_LINEA:[tiempo_inicio,tiempo_final,segmento]} ##

## Obtener las queries y los segmentos, preprocesarlos, extraer caracteristicas partiendo del modelo W2V de la wiki y almacenarlos ##
"""queries  = Utils.get_queries_dict(QUERIES_PATH)
queries  = Preprocess.normalize_queries_dict(queries,remove_duplicates=False,remove_alfanumeric=False)
segments = Utils.get_segments_dict(SEGMENT_PATH)
segments = Preprocess.normalize_segments_dict(segments,remove_duplicates=False,remove_alfanumeric=False)
w2vmodel = Word2Vec.load("en.wiki.w2v.model")
queries = FeatureExtractor.get_queries_dict_representation(queries,w2vmodel,Config.SENTENCE_REPRESENTATION,400) ##### ACORDARSE DE PONER EN TODAS LAS DEMAS APLICACIONES EL NUEVO PARAMETRO CON EL TAMAÑO DEL W2V #####
segments = FeatureExtractor.get_segments_dict_representation(segments,w2vmodel,Config.SENTENCE_REPRESENTATION,400)
Utils.serialize("segments_dict.dump",segments)
Utils.serialize("queries_dict.dump",queries)"""


## Calcular similitudes entre segmentos y queries y almacenarlas ##
"""
queries   = Utils.unserialize("queries_dict.dump")[0]
segments  = Utils.unserialize("segments_dict.dump")[0]
distances = {}
for key1 in segments:
    distances[key1] = []
    for key2 in queries:
	distances[key1].append((key2,NeighboursClassifier.distance_representations(segments[key1][2],queries[key2][3])))
Utils.serialize("distances_dict.dump",distances)"""

## Cargar las distancias y almacenar en el formato correcto ##
distances       = Utils.unserialize("distances_dict.dump")[0]
distances["utep_000.txt_77"] = sorted(distances["utep_000.txt_77"],key=lambda(s,t): int(s.split("_")[1]))
print distances["utep_000.txt_77"]
#segmentsperfile = [79, 163, 171, 99, 236, 135, 120, 134, 156, 153, 150, 137, 116, 195, 133, 195, 455, 148, 122, 262]
#c = 0


## Conocer el numero de segmentos por fichero ##
#aux = files[0][0:files[0].rfind(".")]
#nlines = [1]
#auxc = 1
#for i in xrange(1,len(files)):
#    if aux in files[i]:
#	auxc+=1
#	nlines[-1] = auxc	
#    else:
#	auxc = 1
#	aux = files[i][0:files[i].rfind(".")]
#	nlines.append(auxc)
#print nlines
#############################################
#[79, 163, 171, 99, 236, 135, 120, 134, 156, 153, 150, 137, 116, 195, 133, 195, 455, 148, 122, 262]#



