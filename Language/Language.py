#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.insert(0, '../')
from Kernel import Config
from Kernel import Preprocess
from Kernel import NeighboursClassifier
from Kernel import ClusteringClassifier
from Kernel import FeatureExtractor
from Kernel import Optimizer
from Kernel import Statistics
from Kernel import Utils
from Kernel import Graphic
from Kernel import Messages
from gensim.models import Word2Vec
import logging


"""					DETECTION					"""

CORPUS_TRAIN = "./corpus_language/Train"
CORPUS_TEST  = "./corpus_language/Test"

if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 

## SAVE MODEL ##
#train_sentences = Utils.get_sentences_supervised_from_files(CORPUS_TRAIN)
#test_sentences  = Utils.get_sentences_supervised_from_files(CORPUS_TEST)
#train_sentences = Preprocess.normalize_sentences_supervised(train_sentences,True,False,False,False,False,True,1,non_alfanumeric_func=2)
#test_sentences = Preprocess.normalize_sentences_supervised(test_sentences,True,False,False,False,False,True,1,non_alfanumeric_func=2)


### No supervisado experimento de los kgramas ###
train_sentences = Utils.get_sentences_unsupervised_from_files(CORPUS_TRAIN)
train_sentences = Preprocess.normalize_sentences_unsupervised(train_sentences,True,False,False,False,False,True,1,non_alfanumeric_func=2)
model = Word2Vec(train_sentences,min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V,negative=Config.NEG_W2V)
repr_sentences = FeatureExtractor.get_sentences_representation_unsupervised(train_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
classifier = Config.CLUSTERING_CLASSIFIER(n_clusters=Config.N_CLUSTERS)
print Messages.extracting_clustering_classes
classes = ClusteringClassifier.get_classes(classifier,train_sentences,repr_sentences,3)
print Messages.extracted_classes,classes
#classes = ClusteringClassifier.get_learnt_classes(classifier,repr_sentences)
#Graphic.plot_supervised(repr_sentences,classes)
### GRAFICAR ###
#model = Word2Vec([sentence for (sentence,cat) in train_sentences],min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V,negative=Config.NEG_W2V)
#repr_train_samples  = FeatureExtractor.get_sentences_representation_supervised(train_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
#X,Y = Graphic.unzip_2d(repr_train_samples)
#Graphic.plot_supervised(X,Y)
################

#train_sentences = Preprocess.normalize_sentences_supervised(train_sentences,True,False,False,False,False,True,1,non_alfanumeric_func=2)
#test_sentences = Preprocess.normalize_sentences_supervised(test_sentences,True,False,False,False,False,True,1,non_alfanumeric_func=2)
#clf = Config.SUPERVISED_CLASSIFIERS["DECISION_TREE"]()
#acc = Statistics.partition_supervised(train_sentences,test_sentences,clf,fnormalization=Utils.normalize_norm_supervised_samples) # Leaving one out con algun clasificador de scikit #
#print acc

## Estadisticas ##
#for f in [FeatureExtractor.get_sentence_representation_sum,FeatureExtractor.get_sentence_representation_centroid,FeatureExtractor.get_sentence_representation_centroid_word]:
#    Config.SENTENCE_REPRESENTATION = f
#    print "Probando funcion: ",f
#    for key in Config.SUPERVISED_CLASSIFIERS_FOR_TEST:
#	clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[key]
#	acc = Statistics.partition_supervised(train_sentences,test_sentences,clf,fnormalization=Utils.normalize_norm_supervised_samples) # Leaving one out con algun clasificador de scikit #
#	print key," --> ",str(acc)
#sentences = FeatureExtractor.get_sentences_representation_supervised(sentences,model,Config.SENTENCE_REPRESENTATION)
#Utils.serialize("dest.m",model)
#Utils.serialize("sentences.m",sentences)
################

## TESTING PROJECT CLASSIFIER ##
#model = Utils.unserialize("dest.m")[0]
#sentences = Utils.unserialize("sentences.m")[0]
#sentence  = "ahora tu te vas asi como si nada"
#sentence = Preprocess.normalize(sentence,True,False,False,False,False,True,1)
#sentence = Config.SENTENCE_REPRESENTATION(sentence,model)
#print NeighboursClassifier.get_class(sentence,sentences,1)
################################

#### No supervisado ####







"""					TRADUCTION					"""
