# -*- coding: utf-8 -*-
import azure_translate_api
import sys
import logging
sys.path.insert(0, '../')
from Kernel import Preprocess
client = azure_translate_api.MicrosoftTranslatorClient('XXXX', 'XXXX') # replace the client secret with the client secret for you app.

train_sentences = ["Un usuario nuevo es un miembro valioso de la comunidad con un potencial que no podemos medir, y uno de nuestros recursos mas estimables",
	     "Si realmente deseamos que llegue a colaborar duradera y fructiferamente con el proyecto, debemos recordar que nada ahuyenta con mas rapidez a un recien llegado que la hostilidad, el elitismo, o la sensacion de que nadie esta dispuesto a explicarle de que tratan los problemas",
	     "Algunos usuarios entienden desde un primer momento el espiritu y las normas del proyecto; otros, por la razon que fuere, pueden beneficiarse de la paciencia y gentileza de quienes conocen mejor el terreno",
	     "Al explicar o sugerir cosas a un novato, es preferible relajar el tono aun mas de lo que se considera apropiado",
	     "No supongas que el recién llegado actúa con mala fe; es muy probable que los errores que comete se deban a la inexperiencia, y que sinceramente este decidido a colaborar",
	     "Si las apariencias te hacen pensar que se trata de malicia, haz el intento de atribuirlo a un error benevolente, y explicale cuidadosa y amablemente por que no se debe actuar asi",
	     "Yo intento que haya riesgo cero y espero que esten con nosotros al cien por cien el miercoles, es lo unico que tengo en la cabeza, pero no lo puedo asegurar",
	     "Se recupera muy rapido y a lo mejor hay otros a los que les cuesta mas",
	     "Por ejemplo, cuando yo jugaba me costa más recuperarme y a el le cuesta mucho menos",
	     "Lo importante ahora mismo es que puede estar listo para el partido del miercoles",
	     "Su nombre no puede estar mas en consonancia con las increibles habilidades de este labrador entrenado para detectar los niveles de azucar en personas diabeticas",
	     "antiguos compuestos de rocas terrestres, es probable que se formaran en crateres de impacto de asteroides que salpicaron nuestro naciente planeta, y no por la tectonica de placas",
	     "Sin embargo, solo podemos ir muy atras en el tiempo con el unico material que tenemos",
	     "Hace diez años, un equipo de investigadores argumento que los antiguos cristales de circon probablemente se formaron cuando las placas tectonicas que se mueven alrededor de la superficie",
	     "Lo que encontramos fue bastante sorprendente. Mucha gente penso que los muy antiguos cristales de circon no podrían haberse formado en los crateres de impacto, pero ahora sabemos que podria ser asi",
	     "que encaja con la idea de que nuestro planeta sufrio un bombardeo de asteroides mucho mas intensivo que en tiempos relativamente recientes",
	     "Este descubrimiento arroja luz sobre los origenes de la capacidad de aprendizaje durante la evolucion, incluso antes de la aparicion de un sistema nervioso y el cerebro",
	     "Tambien puede plantear cuestiones acerca de las capacidades de aprendizaje de otros organismos muy simples, tales como virus y bacterias",
	     "Esta facultad se considera generalmente que es una prerrogativa de los organismos dotados de un cerebro y el sistema nervioso",
	     "Sin embargo, los organismos unicelulares tambien tienen que adaptarse al cambio",
	     "De este modo, un equipo de biilogos trató de encontrar pruebas de que un organismo unicelular puede aprender",
	     "La celula aprendio a no temer una sustancia inocua despues de ser confrontada con ella en varias ocasiones, un fenomeno al que los cientificos se refieren como la habituacion",
	     "un protista habituado a la cafeina mostro un comportamiento desconfiado hacia la quinina, y viceversa. La habituacion era, por tanto, claramente especifica a una sustancia dada"]
#words = 0
#for i in xrange(len(train_sentences)):
#    words += len(train_sentences[i])
#print float(words)/len(train_sentences)
text = client.TranslateText(u"{$:holá}", 'es', "en")
print text
"""
test_sentences = ["Eso de que los nuevos sistemas de realidad virtual están pensados para ofrecer una experiencia más realista, profunda y demás monsergas queda muy bien en las notas de prensa, pero nosotros esperamos más",
		  "Pero qué queréis que os diga, si a mí me planteas la posibilidad de salvar a un gato de caer despeñado por un rascacielos, quédate tú con tus épicas batallas espaciales",
		  "Su misión será la de evitar por todos los medios posibles que se vuelvan a producir los terribles sucesos del pasado",
		  "Tal es la situación que los humanos con aumentos mecánicos son considerados por la sociedad como proscritos",
		  "Como el futbolista que se dedica a crear bodegas de vino para sobrevivir a su carrera cuando las piernas no den para más, las compañías de videojuegos también buscan otras alternativas de negocio para que mantenerse a flote no sólo dependa de los juegos",
		  "A algunas les viene de lejos, con negocios que ya existían antes de ponerse a programar, otras buscan en el apoyo de su nombre una nueva forma de invertir lo ganado en el sector del videojuego",
		  "actualmente está disponible hasta el tercero de los cinco que formarán el arco argumental de la aventura completa",
		  "se pusieron de acuerdo para volver al mismo tiempo, con el fin de revitalizar un duelo que en su momento propició millones en ventas y una auténtica pasión desenfrenada"]
translator
langs = ["en","ca","it","fr","de"]
#names = ["Ingles2.txt","Catalan2.txt","Italiano2.txt","Frances2.txt","Aleman2.txt"]
names = ["Ingles.txt","Catalan.txt","Italiano.txt","Frances.txt","Aleman.txt"]
#for i in xrange(len(names)):
#    fd = open("./corpus_language/Train/"+names[i],"r")
#    fo = open("./corpus_language/Train/"+dests[i],"w")
#    for line in fd.readlines():
#	translator.translate("Hello", "pt")
#	line = line.replace("\"","")
#	fo.write(line)
#    fd.close()
#    fo.close()
for i in xrange(len(langs)):
    fd = open("./corpus_language/Test/"+names[i],"a")
    print "Processing: ",langs[i]
    for s in test_sentences:
	text = client.TranslateText(s, 'es', langs[i]).replace("\"","")
	#f = {"q":s}
	#query = "https://statickidz.com/scripts/traductor/?&source=es&target=%s&%s"%(langs[i],urllib.urlencode(f))
	#req = urllib2.Request(query)
	#r   = urllib2.urlopen(req)
	#text = json.load(r)
	fd.write(text+"\n")
    fd.close()
"""
