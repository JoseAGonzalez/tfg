#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.insert(0, '../')
from Kernel import Config
from Kernel import Preprocess
from Kernel import NeighboursClassifier
from Kernel import ClusteringClassifier
from Kernel import FeatureExtractor
from Kernel import Optimizer
from Kernel import Statistics
from Kernel import Graphic
from Kernel import Utils
from Kernel import Messages
from gensim.models import Word2Vec
import logging

model = Word2Vec.load("es.wiki.w2v.model")
print "Model loaded"
print model.similarity("edificio","construccion")
