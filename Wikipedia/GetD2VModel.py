#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import os.path
import sys
from gensim.models.doc2vec import LabeledSentence
from gensim.models import Doc2Vec
from random import shuffle

if __name__ == '__main__':
    program = os.path.basename(sys.argv[0])
    logger = logging.getLogger(program)
 
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s')
    logging.root.setLevel(level=logging.INFO)
    logger.info("running %s" % ' '.join(sys.argv))
 
    # check and process input arguments
 
    if len(sys.argv) < 3:
        print globals()['__doc__'] % locals()
        sys.exit(1)
    inp, outp = sys.argv[1:3]
    with open(inp,"r") as fd:
	i = 0
	train_labeled_sentences = []
	for line in fd.readlines():
	    train_labeled_sentences.append(LabeledSentence(line,tags=[str(i)]))
	    if (i % 10000 == 0): logger.info("Saved " + str(i) + " articles")
	    i += 1
	fd.close()
    model = Doc2Vec(size=400, window=5, min_count=50, workers=8, dm=0, hs=0, dbow_words=1, dm_concat=0)
    model.build_vocab(train_labeled_sentences)
    model.train(train_labeled_sentences)
    model.save("test.wiki.2.model")
