#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging
import os.path
import sys
import multiprocessing
 
from gensim.corpora import  WikiCorpus
from gensim.models import Word2Vec
from gensim.models.word2vec import LineSentence
 
 
if __name__ == '__main__':
    program = os.path.basename(sys.argv[0])
    logger = logging.getLogger(program)
 
    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s')
    logging.root.setLevel(level=logging.INFO)
    logger.info("running %s" % ' '.join(sys.argv))
 
    # check and process input arguments
 
    if len(sys.argv) < 3:
        print globals()['__doc__'] % locals()
        sys.exit(1)
    inp, outp = sys.argv[1:3]
 
    ## Entrenar la primera vez ##
    model  = Word2Vec(LineSentence(inp),workers=8,sg=1,hs=0,size=800,window=50) # Por defecto
    #model = Word2Vec(LineSentence(inp), size=800, window=100, min_count=50, workers=8,hs=0,sg=0,negative=5)
    #############################
    
    ## Cargar el modelo ya entrenado y reentrenar con mas articulos ##
    #model = Word2Vec.load("en.wiki.w2v.model")
    #model.train(LineSentence(inp))
    ##################################################################
    
    # trim unneeded model memory = use (much) less RAM
    model.init_sims(replace=True)
 
    model.save(outp)
