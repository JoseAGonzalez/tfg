from __future__ import absolute_import, print_function

import tweepy
import time
# == OAuth Authentication ==
#


# The consumer keys can be found on your application's Details
# page located at https://dev.twitter.com/apps (under "OAuth settings")
consumer_key="XXXX"
consumer_secret="XXXX"

# The access tokens can be found on your applications's Details
# page located at https://dev.twitter.com/apps (located
# under "Your access token")
access_token="XXXX"
access_token_secret="XXXX"

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.secure = True
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth)

## Extraer todos los tweets del fichero de LLuis ##
NONE = open("NONE.txt","a")
P    = open("P.txt","a")
PP   = open("PP.txt","a")
N    = open("N.txt","a")
NP   = open("NP.txt","a")
NEU  = open("NEU.txt","a")
TRAIN = open("tweets_train.txt","r")
aux = None
text = None
for line in TRAIN.readlines():
    aux   = line.split("\t")
    token = aux[0].strip()
    c     = aux[1].strip()
    time.sleep(0.05)
    try:
	text  = api.get_status(token).text
	if c=="NONE": NONE.write(text+"\n")
	elif c=="P": P.write(text+"\n")
	elif c=="PP": PP.write(text+"\n")
	elif c=="N": N.write(text+"\n")
	elif c=="NP": NP.write(text+"\n")
	elif c=="NEU": NEU.write(text+"\n")
    except: continue

    
NONE.close()
P.close()
PP.close()
N.close()
NP.close()
NEU.close()
TRAIN.close()

