#!/usr/bin/env python
# -*- coding: utf-8 -*-
import codecs
fd = codecs.open("LexiconElhPolar.lex","r",encoding='utf-8')
fp = codecs.open("POS.lex","w",encoding='utf-8')
fn = codecs.open("NEG.lex","w",encoding='utf-8')
positive,negative = 0,0
for line in fd.readlines():
    aux = line.split("\t")
    try:
	lemma = aux[0].strip()
	polar = aux[1].strip()
	if "_" not in lemma:
	    if polar=="negative":
		negative += 1
		fn.write(lemma+"\n")
	    else:
		positive += 1
		fp.write(lemma+"\n")
    except: continue
print "Positivos:",positive
print "Negativos:",negative
fd.close()
fp.close()
fn.close()
