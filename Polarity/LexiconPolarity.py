#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.insert(0, '../')
from Kernel import Config
from Kernel import Utils
from Kernel import SentimentAnalysis
from gensim.models import Word2Vec
import logging

POS_FILE = "./Lexicon/POS.lex"
NEG_FILE = "./Lexicon/NEG.lex"

if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 

## Extraer POS y NEG originales ##
POS,NEG = Utils.get_terms_from_lexicon(POS_FILE,NEG_FILE)
nPOS,nNEG = len(POS),len(NEG)
print nPOS
print nNEG
## 10% de los lemas a test ##
TEST_POS = POS[0:235]
POS      = POS[235:]
TEST_NEG = NEG[0:235]
NEG      = NEG[235:]
##################################

model = Word2Vec.load("es.wiki.w2v.window150.model")
pos_results = SentimentAnalysis.extract_continuous_polarity_from_list(TEST_POS,POS,NEG,model)
neg_results = SentimentAnalysis.extract_continuous_polarity_from_list(TEST_NEG,POS,NEG,model)
#Utils.serialize("pos_test_results",SentimentAnalysis.extract_continuous_polarity_from_list(TEST_POS,POS,NEG,model))
#Utils.serialize("neg_test_results",SentimentAnalysis.extract_continuous_polarity_from_list(TEST_NEG,POS,NEG,model))
#pos_results = Utils.unserialize("pos_test_results")[0]
#neg_results = Utils.unserialize("neg_test_results")[0]
#print pos_results
for (s,t) in neg_results:
    if t>0:
	print (s,t)
	raw_input()
cp,cn = len([1 for (s,t) in pos_results if t>=0]),len([1 for (s,t) in neg_results if t<0])
print cp,cn
