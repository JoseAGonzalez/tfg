#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.insert(0, '../')
from Kernel import Config
from Kernel import Preprocess
from Kernel import NeighboursClassifier
from Kernel import ClusteringClassifier
from Kernel import FeatureExtractor
from Kernel import Optimizer
from Kernel import Statistics
from Kernel import Graphic
from Kernel import Utils
from Kernel import Messages
from gensim.models import Word2Vec
from random import shuffle
import logging


if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 


CORPUS_TRAIN = "./corpusTCstarJuntado2/training"
CORPUS_TEST  = "./corpusTCstarJuntado2/testJuntado"

#print Messages.loading_sentences + " (Train) "
#train_sentences = Utils.get_sentences_supervised_from_folders(CORPUS_TRAIN)
#print Messages.preprocessing_sentences + " (Train) "
#train_sentences = Preprocess.normalize_sentences_supervised(train_sentences,True,False,False,False,True,True,1)
#shuffle(train_sentences)
#print "Cargando modelo W2V"
#model = Word2Vec.load("es.wiki.w2v.window150.model")
#print "W2V cargado"

#B = [  
#       train_sentences[:(int)(len(train_sentences)*0.2)],
#       train_sentences[(int)(len(train_sentences)*0.2):(int)((len(train_sentences)*0.2)*2)],
#       train_sentences[(int)((len(train_sentences)*0.2)*2):(int)((len(train_sentences)*0.2)*3)],
#       train_sentences[(int)((len(train_sentences)*0.2)*3):(int)((len(train_sentences)*0.2)*4)],
#       train_sentences[(int)((len(train_sentences)*0.2)*4):]
#    ]

#Utils.serialize("sentences_tunning_experiment_shuffled",B)
#B = Utils.unserialize("sentences_tunning_experiment_shuffled")[0]
#print len(B[0]),len(B[1]),len(B[2]),len(B[3]),len(B[4]),(len(B[0])+len(B[1])+len(B[2])+len(B[3])+len(B[4]))


## Plot ##
#repr_B = []
#SENTENCE_REPRESENTATION = FeatureExtractor.get_sentence_representation_centroid_word
#SIZE_W2V = 400
#for i in xrange(len(B)): repr_B.append(Utils.normalize_norm_supervised_samples(FeatureExtractor.get_sentences_representation_supervised(B[i],model,SENTENCE_REPRESENTATION,SIZE_W2V)))
#original = [(s,0) for (s,t) in repr_B[0]+repr_B[1]+repr_B[2]+repr_B[3]+repr_B[4]]
#X1,Y1 = Utils.unzip_2d(original)
#Graphic.plot_supervised(X1,Y1)
#tunning_0  = [(s,1) for (s,t) in repr_B[0]]+[(s,0) for (s,t) in repr_B[1]+repr_B[2]+repr_B[3]+repr_B[4]]
#X2,Y2 = Utils.unzip_2d(tunning_0)
#Graphic.plot_supervised(X2,Y2)
#tunning_1  = [(s,0) for (s,t) in repr_B[0]+repr_B[2]+repr_B[3]+repr_B[4]]+[(s,1) for (s,t) in repr_B[1]]
#X2,Y2 = Utils.unzip_2d(tunning_1)
#Graphic.plot_supervised(X2,Y2)
#tunning_2  = [(s,0) for (s,t) in repr_B[0]+repr_B[1]+repr_B[3]+repr_B[4]]+[(s,1) for (s,t) in repr_B[2]]
#X2,Y2 = Utils.unzip_2d(tunning_2)
#Graphic.plot_supervised(X2,Y2)
#tunning_3  = [(s,0) for (s,t) in repr_B[0]+repr_B[1]+repr_B[2]+repr_B[4]]+[(s,1) for (s,t) in repr_B[3]]
#X2,Y2 = Utils.unzip_2d(tunning_3)
#Graphic.plot_supervised(X2,Y2)
#tunning_4  = [(s,0) for (s,t) in repr_B[0]+repr_B[1]+repr_B[2]+repr_B[3]]+[(s,1) for (s,t) in repr_B[4]]
#X2,Y2 = Utils.unzip_2d(tunning_4)
#Graphic.plot_supervised(X2,Y2)
###########

"""
## Tunning W2V ##
fd = open("ResultsTunning","w")
SIZE_W2V = 400
for SENTENCE_REPRESENTATION in [FeatureExtractor.get_sentence_representation_sum,FeatureExtractor.get_sentence_representation_centroid_word]:
    print "------",SENTENCE_REPRESENTATION,"------"
    fd.write("------"+str(SENTENCE_REPRESENTATION)+"------\n\n")
    repr_B = []
    for i in xrange(len(B)):
	repr_B.append(FeatureExtractor.get_sentences_representation_supervised(B[i],model,SENTENCE_REPRESENTATION,SIZE_W2V))
    for i in xrange(len(B)):
	train_samples   = []
	tunning_samples = []
	for j in xrange(len(B)):
	    if j==i: tunning_samples = repr_B[j]
	    else:    train_samples  += repr_B[j]
	train_samples   = Utils.normalize_norm_supervised_samples(train_samples)
	tunning_samples = Utils.normalize_norm_supervised_samples(tunning_samples)
	print "Con bloque",i,"de tunning y los demas train:"
	fd.write("Con bloque "+str(i)+" de tunning y los demas train:\n")
	for key in Config.SUPERVISED_CLASSIFIERS_FOR_TEST: 
	    clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[key]
	    acc = Statistics.partition_supervised_repr(train_samples,tunning_samples,clf)
	    print "\t Clasificador:",key,"-->",acc
	    fd.write("\t Clasificador: "+str(key)+" --> "+str(acc)+"\n")
fd.close()
##################
"""

## Tunning TFIDF ##
"""
print Messages.loading_sentences + " (Train) "
train_sentences = Utils.get_sentences_supervised_from_folders(CORPUS_TRAIN)
print Messages.preprocessing_sentences + " (Train) "
train_sentences = Preprocess.normalize_sentences_supervised(train_sentences,True,False,False,False,True,True,1)
shuffle(train_sentences)
Utils.serialize("sentences_tunning_experiment_shuffled_discrete",train_sentences)
"""

"""
train_sentences = Utils.unserialize("sentences_tunning_experiment_shuffled_discrete")[0]
XT,YT = Utils.unzip_2d(train_sentences)
XT = Utils.sentences_from_tokenized(XT)
tfidf       = FeatureExtractor.get_tf_idf_from_bow(FeatureExtractor.get_bow_from_sentences(XT)).toarray()
XT          = tfidf[:]
train_sentences = Utils.zip_2d(XT,YT)
del(XT)
del(YT)
repr_B = [  
		train_sentences[:(int)(len(train_sentences)*0.2)],
		train_sentences[(int)(len(train_sentences)*0.2):(int)((len(train_sentences)*0.2)*2)],
		train_sentences[(int)((len(train_sentences)*0.2)*2):(int)((len(train_sentences)*0.2)*3)],
		train_sentences[(int)((len(train_sentences)*0.2)*3):(int)((len(train_sentences)*0.2)*4)],
		train_sentences[(int)((len(train_sentences)*0.2)*4):]
	 ]
del(train_sentences)

## Plot ##
#print "Extracting original"
#original = [(s,0) for (s,t) in repr_B[0]+repr_B[1]+repr_B[2]+repr_B[3]+repr_B[4]]
#X1,Y1 = Utils.unzip_2d(original)
#print "Plotting"
#Graphic.plot_supervised(X1,Y1)
#tunning_0  = [(s,1) for (s,t) in repr_B[0]]+[(s,0) for (s,t) in repr_B[1]+repr_B[2]+repr_B[3]+repr_B[4]]
#X2,Y2 = Utils.unzip_2d(tunning_0)
#Graphic.plot_supervised(X2,Y2)
#tunning_1  = [(s,0) for (s,t) in repr_B[0]+repr_B[2]+repr_B[3]+repr_B[4]]+[(s,1) for (s,t) in repr_B[1]]
#X2,Y2 = Utils.unzip_2d(tunning_1)
#Graphic.plot_supervised(X2,Y2)
#tunning_2  = [(s,0) for (s,t) in repr_B[0]+repr_B[1]+repr_B[3]+repr_B[4]]+[(s,1) for (s,t) in repr_B[2]]
#X2,Y2 = Utils.unzip_2d(tunning_2)
#Graphic.plot_supervised(X2,Y2)
#tunning_3  = [(s,0) for (s,t) in repr_B[0]+repr_B[1]+repr_B[2]+repr_B[4]]+[(s,1) for (s,t) in repr_B[3]]
#X2,Y2 = Utils.unzip_2d(tunning_3)
#Graphic.plot_supervised(X2,Y2)
#tunning_4  = [(s,0) for (s,t) in repr_B[0]+repr_B[1]+repr_B[2]+repr_B[3]]+[(s,1) for (s,t) in repr_B[4]]
#X2,Y2 = Utils.unzip_2d(tunning_4)
#Graphic.plot_supervised(X2,Y2)
###########
"""

##########	 
"""
fd = open("ResultsTunningDiscrete","w")
for i in xrange(len(repr_B)):
    train_samples   = []
    tunning_samples = []
    for j in xrange(len(repr_B)):
	if j==i: tunning_samples = repr_B[j]
	else:    train_samples  += repr_B[j]
    #train_samples   = Utils.normalize_norm_supervised_samples(train_samples)
    #tunning_samples = Utils.normalize_norm_supervised_samples(tunning_samples)
    print "Con bloque",i,"de tunning y los demas train:"
    fd.write("Con bloque "+str(i)+" de tunning y los demas train:\n")
    for key in Config.SUPERVISED_CLASSIFIERS_FOR_TEST: 
	clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[key]
	print "\t Clasificador:",key,"-->",
	acc = Statistics.partition_supervised_repr(train_samples,tunning_samples,clf)
	print acc
	fd.write("\t Clasificador: "+str(key)+" --> "+str(acc)+"\n")
fd.close()
"""

"""
print Messages.loading_sentences + " (Test) "
test_sentences  = Utils.get_sentences_supervised_from_folders(CORPUS_TEST)
print Messages.preprocessing_sentences + " (Test) "
test_sentences  = Preprocess.normalize_sentences_supervised(test_sentences,True,False,False,False,True,True,1)
"""
