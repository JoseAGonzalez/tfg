#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import logging
sys.path.insert(0, '../')
from Kernel import Config
from Kernel import Preprocess
from Kernel import NeighboursClassifier
from Kernel import ClusteringClassifier
from Kernel import FeatureExtractorDBpediaProject
from Kernel import FeatureExtractorDBpediaSpotlight
from Kernel import Optimizer
from Kernel import StatisticsDBpedia
from Kernel import Utils
from Kernel import Messages
from Kernel import Graphic

#p = Utils.unserialize("properties.dump")[0]
#print p
############################ Testing dbpedia spotlight ############################
CORPUS = "./corpus_topics"
#train_sentences = Utils.get_sentences_supervised_from_files(CORPUS)
#train_sentences = Preprocess.normalize_sentences_supervised(train_sentences,False,False,False,False,False,True,1)
#repr_train_sentences = FeatureExtractorDBpediaSpotlight.get_sentences_representation_supervised(train_sentences,Config.SENTENCE_REPRESENTATION_SPOTLIGHT_DBPEDIA,Config.DBCLASSES,0,confidence=0.8)
#Utils.serialize("repr_train_sentences_corpus_topics_aditivo_booleano_0.8",repr_train_sentences)
#repr_train_sentences = Utils.unserialize("repr_train_sentences_corpus_topics_aditivo_sum_0.8")[0]

## Graficar las muestras segun los vectores ##
#X,Y = Graphic.unzip_2d(Utils.normalize_norm_supervised_samples(repr_train_sentences))
#Graphic.plot_supervised(X,Y)
##############################################

############################ END Testing dbpedia spotlight ############################

## Obtener representaciones del corpus 1 (TOPICS), no separacion entre TEST y TRAIN ##
#CORPUS = "./corpus_topics/"
#train_sentences = Utils.get_sentences_supervised_from_files(CORPUS)
#train_sentences = Preprocess.normalize_sentences_supervised(train_sentences,True,False,False,False,True,True,1,dbpedia=True)

## repr_[train/test]_sentences_mode_[0 -> booleano, 1 -> suma de apariciones]_[sum|mult|distance|weighted|covariance]_dbpedia_topics_[nº corpus].dump ##


# Guardando representaciones con modo booleano y combinación suma del corpus 1 #
#repr_train_sentences = FeatureExtractorDBpediaProject.get_sentences_representation_supervised(train_sentences,Config.SENTENCE_REPRESENTATION_DBPEDIA,Config.DBCLASSES,0)

#Utils.serialize("repr_train_sentences_mode_0_sum_dbpedia_topics_1.dump",repr_train_sentences)

#print "Corpus Topics con modo booleano y combinacion suma del corpus 1 finalizado!"
## Graficar las muestras segun los vectores ##
#repr_train_sentences = Utils.unserialize("repr_train_sentences_mode_0_sum_dbpedia_topics_1.dump")[0]
#X,Y = Graphic.unzip_2d(Utils.normalize_norm_supervised_samples(repr_train_sentences))
#Graphic.plot_supervised(X,Y)
##############################################
# Guardando representaciones con modo suma de apariciones y combinación suma del corpus 1 #
#print train_sentences[2]
#repr_train_sentences = FeatureExtractorDBpediaProject.get_sentences_representation_supervised(train_sentences,Config.SENTENCE_REPRESENTATION_DBPEDIA,Config.DBCLASSES,1)

#Utils.serialize("repr_train_sentences_mode_1_sum_dbpedia_topics_1.dump",repr_train_sentences)

#print "Corpus Topics con modo suma de apariciones y combinacion suma del corpus 1 finalizado!"
## Graficar las muestras segun los vectores ##
#repr_train_sentences = Utils.unserialize("repr_train_sentences_mode_1_sum_dbpedia_topics_1.dump")[0]
#X,Y = Graphic.unzip_2d(Utils.normalize_norm_supervised_samples(repr_train_sentences))
#Graphic.plot_supervised(X,Y)
##############################################
#StatisticsDBpedia.partition_nearest(train_sentences,test_sentences,1,0)

## END Obtener representaciones del corpus 1 (TOPICS), no separacion entre TEST y TRAIN ##

## Obtener representaciones del corpus TCSTAR (TEST y TRAIN separados) ##
"""CORPUS_TRAIN = "./corpusTCstar/training"
CORPUS_TEST = "./corpusTCstar/test"

train_sentences = Utils.get_sentences_supervised_from_folders(CORPUS_TRAIN)
train_sentences = Preprocess.normalize_sentences_supervised(train_sentences,True,False,False,False,True,True,1,dbpedia=True)
test_sentences  = Utils.get_sentences_supervised_from_folders(CORPUS_TEST)
test_sentences = Preprocess.normalize_sentences_supervised(test_sentences,True,False,False,False,True,True,1,dbpedia=True)


## repr_[train/test]_sentences_mode_[0 -> booleano, 1 -> suma de apariciones]_[sum|mult|distance|weighted|covariance]_dbpedia_topics_[nº corpus].dump ##

# Guardando representaciones con modo booleano y combinación suma del corpus 2 #
print train_sentences[-1]
raw_input()
repr_train_sentences = FeatureExtractorDBpediaProject.get_sentences_representation_supervised(train_sentences,Config.SENTENCE_REPRESENTATION_DBPEDIA,Config.DBCLASSES,0)
repr_test_sentences = FeatureExtractorDBpediaProject.get_sentences_representation_supervised(test_sentences,Config.SENTENCE_REPRESENTATION_DBPEDIA,Config.DBCLASSES,0)

Utils.serialize("repr_train_sentences_mode_0_sum_dbpedia_topics_2.dump",repr_train_sentences)
Utils.serialize("repr_test_sentences_mode_0_sum_dbpedia_topics_2.dump",repr_test_sentences)

print "Corpus TCSTAR con modo booleano y combinacion suma del corpus 2 finalizado!"

# Guardando representaciones con modo suma de apariciones y combinación suma del corpus 1 #

repr_train_sentences = FeatureExtractorDBpediaProject.get_sentences_representation_supervised(train_sentences,Config.SENTENCE_REPRESENTATION_DBPEDIA,Config.DBCLASSES,1)
repr_test_sentences = FeatureExtractorDBpediaProject.get_sentences_representation_supervised(test_sentences,Config.SENTENCE_REPRESENTATION_DBPEDIA,Config.DBCLASSES,1)

Utils.serialize("repr_train_sentences_mode_1_sum_dbpedia_topics_2.dump",repr_train_sentences)
Utils.serialize("repr_test_sentences_mode_1_sum_dbpedia_topics_2.dump",repr_test_sentences)

print "Corpus TCSTAR con modo suma de apariciones y combinacion suma del corpus 2 finalizado!"
"""
## Graficar las muestras segun los vectores de train tcstar ##
repr_train_sentences = Utils.unserialize("repr_train_sentences_mode_1_sum_dbpedia_topics_2.dump")[0]
X,Y = Graphic.unzip_2d(Utils.normalize_norm_supervised_samples(repr_train_sentences))
Graphic.plot_supervised(X,Y)
##############################################
## END Obtener representaciones del corpus TCSTAR (TEST y TRAIN separados) ##

## Obtener resultados de clasificacion para el corpus TOPICS (No separacion entre TEST y TRAIN -> Particion de TRAIN y Resustitucion ) ##

# Resultados con modo booleano y combinacion suma #
#repr_sentences = Utils.unserialize("repr_train_sentences_mode_1_sum_dbpedia_topics_1.dump")[0]
repr_sentences = Utils.unserialize("repr_train_sentences_corpus_topics_aditivo_sum_0.8")[0]
from random import shuffle
shuffle(repr_sentences)
## Coger 2/3 partes para train y 1/3 para test
repr_train_sentences = Utils.normalize_norm_supervised_samples(repr_sentences[0:len(repr_sentences)-(len(repr_sentences)/3)])
repr_test_sentences = Utils.normalize_norm_supervised_samples(repr_sentences[len(repr_sentences)-(len(repr_sentences)/3)+1:])
## Partition 2/3 1/3 ##
for key in Config.SUPERVISED_CLASSIFIERS_FOR_TEST:
    print "Probando con ",key
    clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[key]
    acc = StatisticsDBpedia.partition_supervised(repr_train_sentences,repr_test_sentences,clf) # Leaving one out con algun clasificador de scikit #
    print key," --> ",str(acc) 
#######################
## Resustitution ##
#repr_sentences = Utils.normalize_norm_supervised_samples(repr_sentences)
#for key in Config.SUPERVISED_CLASSIFIERS_FOR_TEST:
 #   print "Probando con ",key
 #   clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[key]
 #   acc = StatisticsDBpedia.resustitution_supervised(repr_sentences,clf) # Leaving one out con algun clasificador de scikit #
 #   print key," --> ",str(acc) 
###################

## END Obtener resultados de clasificacion para el corpus TCSTAR (No separacion entre TEST y TRAIN -> Particion de TRAIN ) ##

## Obtener resultados de clasificacion para el corpus TCSTAR (Separacion entre TEST y TRAIN ) ##
#repr_train_sentences = Utils.unserialize("repr_train_sentences_mode_1_sum_dbpedia_topics_2.dump")[0]
#repr_test_sentences = Utils.unserialize("repr_test_sentences_mode_1_sum_dbpedia_topics_2.dump")[0]
#from random import shuffle
#shuffle(repr_test_sentences)
#repr_test_sentences = repr_test_sentences[0:500]
#clf = Config.SUPERVISED_CLASSIFIER(C=1.0, cache_size=200, class_weight=None, coef0=0.5,
 #   decision_function_shape=None, degree=3, gamma='auto', kernel='sigmoid',
 #   max_iter=10, probability=False, random_state=None, shrinking=True,
 #   tol=0.001, verbose=False)
#clf = Config.SUPERVISED_CLASSIFIER(metric="cosine")
#StatisticsDBpedia.partition_supervised(repr_train_sentences,repr_test_sentences,clf)
#StatisticsDBpedia.partition_nearest(repr_train_sentences,repr_test_sentences,1)
