#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.insert(0, '../')
from Kernel import Config
from Kernel import Preprocess
from Kernel import NeighboursClassifier
from Kernel import ClusteringClassifier
from Kernel import FeatureExtractor
from Kernel import Optimizer
from Kernel import Statistics
from Kernel import Graphic
from Kernel import Utils
from Kernel import Messages
from gensim.models import Word2Vec
from gensim.models import Doc2Vec
from numpy import concatenate
import logging


if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 

## Corpus de prueba ##
CORPUS = "./corpus_topics/"
######################

############################# CLUSTERING #############################
## SAVE MODEL CLUSTERING ##
#print Messages.clustering_header
#print Messages.loading_sentences
#sentences = Utils.get_sentences_unsupervised_from_files(CORPUS)
#print Messages.preprocessing_sentences
#sentences = Preprocess.normalize_sentences_unsupervised(sentences,True,False,False,False,True,True,1)
#print Messages.training_w2v
#model     = Word2Vec(sentences,min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V)
#print Messages.extracting_features
#repr_sentences = FeatureExtractor.get_sentences_representation_unsupervised(sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
##########################################################
#Utils.serialize("unsupervised_dest.m",model)
#Utils.serialize("unsupervised_sentences.m",sentences)
#Utils.serialize("unsupervised_representation_sentences.m",repr_sentences)

################################

## TESTING CLUSTERING CLASSSIFIER ##
#model = Utils.unserialize("unsupervised_dest.m")[0]
#repr_sentences = Utils.unserialize("unsupervised_representation_sentences.m")[0]
#sentences = Utils.unserialize("unsupervised_sentences.m")[0]
#test_sentence = "Arsenal team win with 4 goals"
#print Messages.loading_sentences
#sentences = Utils.get_sentences_unsupervised_from_files(CORPUS)
#print Messages.preprocessing_sentences
#sentences = Preprocess.normalize_sentences_unsupervised(sentences,True,False,False,False,True,True,1)
#print Messages.loading_model
## Para clustering con el modelo de la wiki ##
#model = Word2Vec.load("en.wiki.w2v.model")
#print "Model loaded"
##############################################

#model     = Word2Vec(sentences,min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V)
#print Messages.extracting_features
#repr_sentences = Utils.normalize_norm_unsupervised_samples(FeatureExtractor.get_sentences_representation_unsupervised(sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V))
#classifier = Config.CLUSTERING_CLASSIFIER(n_clusters=Config.N_CLUSTERS)
## Testing graphics ##
#classes = ClusteringClassifier.get_learnt_classes(classifier,repr_sentences)
#Graphic.plot_supervised(repr_sentences,classes)
## Testing classify clustering topic by k-gram ##
#print Messages.extracting_clustering_classes
#classes = ClusteringClassifier.get_classes(classifier,sentences,repr_sentences,3)
#print Messages.extracted_classes,classes
#test_sentence = Preprocess.normalize(test_sentence,True,False,False,False,True,True,1)
#test_sentence = Config.SENTENCE_REPRESENTATION(test_sentence,model)
#print Messages.predicted_class,ClusteringClassifier.get_class(classifier,test_sentence,classes)
#####################################################################
#print "\n\n"
"""
"""
############################# SUPERVISED #############################

## SAVE MODEL SUPERVISED ##
#print Messages.supervised_header
#print Messages.loading_sentences
#sentences = Utils.get_sentences_supervised_from_files(CORPUS)
#print Messages.preprocessing_sentences
#sentences = Preprocess.normalize_sentences_supervised(sentences,True,False,False,False,True,True,1)
#print Messages.training_w2v
#model     = Word2Vec([sentence for (sentence,cat) in sentences],min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V)
#print Messages.extracting_features
#repr_sentences = FeatureExtractor.get_sentences_representation_supervised(sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
#### GRAFICAR CORPUS_TOPICS PARA VER COMO SE DISTRIBUYE ###
#X,Y  = Graphic.unzip_2d(repr_sentences)
#X,Y = Graphic.unzip_2d(Utils.normalize_norm_supervised_samples(repr_sentences))
#Graphic.plot_supervised(X,Y)
##########################################################
#Utils.serialize("supervised_dest.m",model)
#Utils.serialize("supervised_repr_sentences.m",repr_sentences)
#Utils.serialize("supervised_sentences.m",sentences)
################################

## Testing english wikipedia model with corpus_topics ##
#model = Word2Vec.load("en.wiki.w2v.model")
#print "Model loaded"
#repr_sentences = Utils.normalize_norm_supervised_samples(FeatureExtractor.get_sentences_representation_supervised(sentences,model,Config.SENTENCE_REPRESENTATION,400))
#print "Fin caracteristicas"
#for key in Config.SUPERVISED_CLASSIFIERS_FOR_TEST:
#    print "Probando con ",key
#    clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[key]
#    acc = Statistics.leaving_one_out_supervised_classifier_other_model(repr_sentences,clf) # Leaving one out con algun clasificador de scikit #
#    print key," --> ",str(acc)

#### GRAFICAR CORPUS_TOPICS PARA VER COMO SE DISTRIBUYE ###
#X,Y  = Graphic.unzip_2d(repr_sentences)
#X,Y = Graphic.unzip_2d(Utils.normalize_norm_supervised_samples(repr_sentences))
#Graphic.plot_supervised(X,Y)
###########################################################
"""
## TESTING PROJECT CLASSIFIER (distancia coseno en clasificacion y representacion suma se comportan mejor en este caso -> 92% acc) ##
model = Utils.unserialize("supervised_dest.m")[0]
repr_sentences = Utils.unserialize("supervised_repr_sentences.m")[0]
sentence  = "Arsenal team win with 4 goals"
print Messages.preprocessing_sentences
sentence = Preprocess.normalize(sentence,True,False,False,False,True,True,1)
print Messages.extracting_features
sentence = Config.SENTENCE_REPRESENTATION(sentence,model,Config.SIZE_W2V)
print Messages.predicted_class,NeighboursClassifier.get_class(sentence,repr_sentences,1)
################################
"""

## STATISTICS ##
#print "\n",Messages.statistics_supervised_header
#print Messages.loading_sentences
#sentences = Utils.get_sentences_supervised_from_files(CORPUS)
#print Messages.preprocessing_sentences
#sentences = Preprocess.normalize_sentences_supervised(sentences,True,False,False,False,True,True,1)
#print "\n\n",Messages.statistics_resustitution_header
#Statistics.resustitution_nearest(sentences,1)
#print "\n\n",Messages.statistics_resustitution_header
#clf = Config.SUPERVISED_CLASSIFIER(metric=Config.DISTANCE_CLASSIFICATION)
#Statistics.resustitution_supervised_classifier(sentences,clf)
#print "\n\n",Messages.statistics_lou_header
#Statistics.leaving_one_out_nearest(sentences,1,fnormalization=Utils.normalize_norm_supervised_samples) # Leaving one out con el clasificador del proyecto #
#print "\n\n",Messages.statistics_lou_header
#clf = Config.SUPERVISED_CLASSIFIER_FOR_TEST

#for key in Config.SUPERVISED_CLASSIFIERS_FOR_TEST:
#    print "Probando con ",key
#    clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[key]
#    acc = Statistics.leaving_one_out_supervised_classifier(sentences,clf,fnormalization=Utils.normalize_norm_supervised_samples) # Leaving one out con algun clasificador de scikit #
#    print key," --> ",str(acc)
################################


## OPTIMIZE PARAMETERS ##
		# ... #
#########################


#print "\n\n"

###################################### Test TCSTAR ######################################


CORPUS_TRAIN = "./corpusTCstarJuntado2/training"
CORPUS_TEST  = "./corpusTCstarJuntado2/testJuntado"

# SUPERVISED - testeando la carpeta test, habiendo entrenado con la carpeta training - #
print Messages.supervised_header
print Messages.loading_sentences + " (Train) "
train_sentences = Utils.get_sentences_supervised_from_folders(CORPUS_TRAIN)
print Messages.loading_sentences + " (Test) "
test_sentences  = Utils.get_sentences_supervised_from_folders(CORPUS_TEST)
print Messages.preprocessing_sentences + " (Train) "

train_sentences = Preprocess.normalize_sentences_supervised(train_sentences,True,False,False,False,True,True,1,stopwords=Config.STOPWORDS_FILE)
### GRAFICAR MUESTRAS DE ENTRENAMIENTO A VER COMO SE DISTRIBUYEN (con el modelo obtenido con el corpus) ###
#model = Word2Vec([sentence for (sentence,cat) in train_sentences],min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V)
#repr_train_sentences = FeatureExtractor.get_sentences_representation_supervised(train_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
#X,Y = Graphic.unzip_2d(Utils.normalize_norm_supervised_samples(repr_train_sentences))
#Graphic.plot_supervised(X,Y)
####################################################################

print Messages.preprocessing_sentences + " (Test) "
test_sentences  = Preprocess.normalize_sentences_supervised(test_sentences,True,False,False,False,True,True,1,stopwords=Config.STOPWORDS_FILE)


###################################### TUNING ####################################

#CORPUS_TRAIN  = "./corpusTCstarTuningPreparado/newTraining"
#CORPUS_TUNING = "./corpusTCstarTuningPreparado/newTuning"
#print Messages.supervised_header
#print Messages.loading_sentences + " (Train) "
#train_sentences = Utils.get_sentences_supervised_from_folders(CORPUS_TRAIN)
#print Messages.loading_sentences + " (Tuning) "
#tuning_sentences  = Utils.get_sentences_supervised_from_folders(CORPUS_TUNING)
#print Messages.preprocessing_sentences + " (Train) "
#train_sentences = Preprocess.normalize_sentences_supervised(train_sentences,True,False,False,False,True,True,1)
#print Messages.preprocessing_sentences + " (Tuning) "
#tuning_sentences  = Preprocess.normalize_sentences_supervised(tuning_sentences,True,False,False,False,True,True,1)
## Entrenar los 4 modelos word2vec ##
#model1 = Word2Vec([s for (s,t) in train_sentences],sg=1,hs=1)
#model1.save("tuning_tcstar_1.model")
#del(model1)
#model2 = Word2Vec([s for (s,t) in train_sentences],sg=1,hs=0)
#model2.save("tuning_tcstar_2.model")
#del(model2)
#model3 = Word2Vec([s for (s,t) in train_sentences],sg=0,hs=1)
#model3.save("tuning_tcstar_3.model")
#del(model3)
#model4 = Word2Vec([s for (s,t) in train_sentences],sg=0,hs=0)
#model4.save("tuning_tcstar_4.model")
#del(model4)
#####################################
#for rep in ["suma","centroide"]:
#    if rep=="suma": r = FeatureExtractor.get_sentence_representation_sum
#    elif rep=="centroide": r = FeatureExtractor.get_sentence_representation_centroid
#    for model_name in ["tuning_tcstar_1.model","tuning_tcstar_2.model","tuning_tcstar_3.model","tuning_tcstar_4.model"]:
#	model = Word2Vec.load(model_name)
#	train_sentences_repr  = FeatureExtractor.get_sentences_representation_supervised(train_sentences,model,r,100)
#	tuning_sentences_repr = FeatureExtractor.get_sentences_representation_supervised(tuning_sentences,model,r,100)
#	for clf_name in Config.SUPERVISED_CLASSIFIERS_FOR_TEST:
#	    clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[clf_name]
#	    print model_name,"repr",rep,"clf",clf_name,"accuracy -->",Statistics.partition_supervised_repr(train_sentences_repr,tuning_sentences_repr,clf)
# VISUALIZACIÓN #
#X,Y = Graphic.unzip_2d(Utils.normalize_norm_supervised_samples(train_sentences_repr))
#Graphic.plot_supervised(X,Y)
###############################################
model = Word2Vec.load("./BarridoParametros/TCStar/tcstar_size_200_window_20.model")
train_sentences_repr  = FeatureExtractor.get_sentences_representation_supervised(train_sentences,model,FeatureExtractor.get_sentence_representation_sum,200)
test_sentences_repr = FeatureExtractor.get_sentences_representation_supervised(test_sentences,model,FeatureExtractor.get_sentence_representation_sum,200)
    #for clf_name in Config.SUPERVISED_CLASSIFIERS_FOR_TEST:
	#clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[clf_name]
clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST["SVM_KERNEL_LINEAR"]
	#print "WIKI + ",rep," + "+clf_name+" --> accuracy: ",Statistics.partition_supervised_repr(train_sentences_repr,tuning_sentences_repr,clf)
        #print "Wiki model 1","+",rep,"+","--> accuracy: ",Statistics.partition_supervised_repr(train_sentences_repr,tuning_sentences_repr,clf)
#print "SUMA CBOW 800, 50, 50, SVM --> accuracy: ",Statistics.partition_supervised_repr(train_sentences_repr,tuning_sentences_repr,clf)
print "Accuracy: ",Statistics.partition_supervised_repr(train_sentences_repr,test_sentences_repr,clf)

###################################################################################
#print test_sentences[0][0][0:5]
## Pruebas wikipedia ##
#model  = Word2Vec.load("es.wiki.size800.window100.mincount50.cbow.ns.model")
#model3 = Word2Vec.load("es.wiki.size800.window5.mincount50.sg.ns.model")
#model3 = Word2Vec.load("wikipedia.500.window10.mincount50.cbow.ns.model") # X
#model2 = Word2Vec.load("es.wiki.size800.window5.mincount50.cbow.ns.model") # X
#model4 = Word2Vec.load("es.wiki.size256.window10.mincount50.hs.ns.model")
#model1 = Word2Vec.load("es.wiki.size800.window50.mincount50.cbow.ns.model") # X
#model3 = Word2Vec.load("es.wiki.w2v.window150.model")
#model1  = Word2Vec.load("es.wiki.size800.window50.mincount50.cbow.ns.model")
#model2  = Word2Vec.load("es.wiki.size800.window5.mincount50.cbow.ns.model")
#model3  = Word2Vec.load("es.wiki.w2v.window150.model")
## CBOW ##
#model1  = Word2Vec.load("es.wiki.size800.window5.mincount50.cbow.ns.model")
#model2  = Word2Vec.load("wikipedia.500.window10.mincount50.cbow.ns.model")
#model5  = Word2Vec.load("es.wiki.size256.window10.mincount50.hs.ns.model")
#model3  = Word2Vec.load("es.wiki.size800.window50.mincount50.cbow.ns.model")
#model4  = Word2Vec.load("es.wiki.w2v.window150.model")
##########
#model3 = Word2Vec([sentence for (sentence,cat) in train_sentences],min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V)
#train_sentences_repr1 = FeatureExtractor.get_sentences_representation_supervised(train_sentences,model1,Config.SENTENCE_REPRESENTATION,800)
#train_sentences_repr2 = FeatureExtractor.get_sentences_representation_supervised(train_sentences,model2,Config.SENTENCE_REPRESENTATION,500)
#train_sentences_repr3 = FeatureExtractor.get_sentences_representation_supervised(train_sentences,model3,Config.SENTENCE_REPRESENTATION,800)
#train_sentences_repr4 = FeatureExtractor.get_sentences_representation_supervised(train_sentences,model4,Config.SENTENCE_REPRESENTATION,400)
#train_sentences_repr5 = FeatureExtractor.get_sentences_representation_supervised(train_sentences,model5,Config.SENTENCE_REPRESENTATION,256)

#train_sentences_repr = [(0.7*train_sentences_repr1[i][0]+0.3*train_sentences_repr2[i][0],train_sentences_repr1[i][1]) for i in xrange(len(train_sentences_repr1))]
#train_sentences_repr  = [(concatenate((1*train_sentences_repr1[i][0],1*train_sentences_repr2[i][0],1*train_sentences_repr3[i][0]),axis=0),train_sentences_repr1[i][1]) for i in xrange(len(train_sentences_repr1))]
#del(train_sentences_repr1)
#del(train_sentences_repr2)
#del(train_sentences_repr3)
#del(train_sentences_repr4)
#del(train_sentences_repr5)

#tuning_sentences_repr1 = FeatureExtractor.get_sentences_representation_supervised(tuning_sentences,model1,Config.SENTENCE_REPRESENTATION,800)
#tuning_sentences_repr2 = FeatureExtractor.get_sentences_representation_supervised(tuning_sentences,model2,Config.SENTENCE_REPRESENTATION,500)
#tuning_sentences_repr3 = FeatureExtractor.get_sentences_representation_supervised(tuning_sentences,model3,Config.SENTENCE_REPRESENTATION,800)
#test_sentences_repr1 = FeatureExtractor.get_sentences_representation_supervised(test_sentences,model1,Config.SENTENCE_REPRESENTATION,800)
#test_sentences_repr2 = FeatureExtractor.get_sentences_representation_supervised(test_sentences,model2,Config.SENTENCE_REPRESENTATION,500)
#test_sentences_repr3 = FeatureExtractor.get_sentences_representation_supervised(test_sentences,model3,Config.SENTENCE_REPRESENTATION,800)
#test_sentences_repr4 = FeatureExtractor.get_sentences_representation_supervised(test_sentences,model4,Config.SENTENCE_REPRESENTATION,400)
#test_sentences_repr5 = FeatureExtractor.get_sentences_representation_supervised(test_sentences,model5,Config.SENTENCE_REPRESENTATION,256)
#test_sentences_repr = [(0.7*tuning_sentences_repr1[i][0]+0.3*tuning_sentences_repr2[i][0],tuning_sentences_repr1[i][1]) for i in xrange(len(test_sentences_repr1))]
# (0.5,0.3,0.2) #
#test_sentences_repr  = [(concatenate((1*test_sentences_repr1[i][0],1*test_sentences_repr2[i][0],1*test_sentences_repr3[i][0],1*test_sentences_repr4[i][0],1*test_sentences_repr5[i][0]),axis=0),test_sentences_repr1[i][1]) for i in xrange(len(test_sentences_repr1))]
#tuning_sentences_repr  = [(concatenate((1*tuning_sentences_repr1[i][0],1*tuning_sentences_repr2[i][0],1*tuning_sentences_repr3[i][0]),axis=0),tuning_sentences_repr1[i][1]) for i in xrange(len(tuning_sentences_repr1))]
#del(tuning_sentences_repr1)
#del(tuning_sentences_repr2)
#del(tuning_sentences_repr3)
#del(test_sentences_repr4)
#del(test_sentences_repr5)
#del(model1)
#del(model2)
#del(model3)
#del(model4)
#del(model5)
#### TRAIN ONLINE W2V ####
#print model.n_similarity([u"españa"],[u"madrid"])
#model.train([s for (s,c) in train_sentences])
#model = Doc2Vec.load("test.wiki.2.model")
#print "Model loaded"
### GRAFICAR MUESTRAS DE ENTRENAMIENTO A VER COMO SE DISTRIBUYEN (con el modelo de la wiki) ###
#repr_train_sentences = FeatureExtractor.get_sentences_representation_supervised(train_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
#X,Y = Graphic.unzip_2d(Utils.normalize_norm_supervised_samples(repr_train_sentences))
#Graphic.plot_supervised(X,Y)
####################################################################
# Testing all wikipedia #
#for f in [FeatureExtractor.get_sentence_representation_sum,FeatureExtractor.get_sentence_representation_centroid,FeatureExtractor.get_sentence_representation_centroid_word]:
#    Config.SENTENCE_REPRESENTATION = f
#    repr_train_sentences = Utils.normalize_norm_supervised_samples(FeatureExtractor.get_sentences_representation_supervised(train_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V))
#    repr_test_sentences  = Utils.normalize_norm_supervised_samples(FeatureExtractor.get_sentences_representation_supervised(test_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V))
#    print "Probando funcion: ",f
#    for key in Config.SUPERVISED_CLASSIFIERS_FOR_TEST:
# 	print key," --> ",
#	clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[key]
#	acc = Statistics.partition_supervised_repr(repr_train_sentences,repr_test_sentences,clf,model) # Leaving one out con algun clasificador de scikit #
#	print str(acc)
#########################
#######################

#print test_sentences[0]
#word_length_train_sentences = [len(sentence) for (sentence,cat) in train_sentences]
#word_length_test_sentences  = [len(sentence) for (sentence,cat) in test_sentences]
#Statistics.leaving_one_out_nearest(train_sentences+test_sentences)
#print Messages.training_w2v
#model = Word2Vec([sentence for (sentence,cat) in train_sentences],min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V)
#Utils.serialize("model_tcstar.m",model)
#Utils.serialize("train_sentences_tcstar.m",train_sentences)
#Utils.serialize("test_sentences_tcstar.m",test_sentences)
#Statistics.partition_nearest(train_sentences,test_sentences,1,model) # Project classifier #
#print "\n\n",Messages.statistics_common_header

## 58.1% acc con tcstar modelo wikipedia (window 75/150, correlation) representacion suma test 252!!! SI NO SE NORMALIZA -> 66.1% SVM LINEAL ##
#clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST["SVM_KERNEL_LINEAR"]
### RECORD-> 68.2% SVM con es.wiki.size800.window5.mincount50.cbow.ns.model (69.8% si no se normaliza!!) -> PROBAR MAS CLASIFICADORES ###
### Con este tambien probar mas wikipedia.500.window10.mincount50.cbow.ns.model (antes de poner a entrenar el nuevo skipgram) ###
### NUEVO RECORD -> 70.7% "es.wiki.size800.window50.mincount50.cbow.ns.model" sin normalizar y normalizando ###
### NUEVO RECORD -> 72.3% "es.wiki.size800.window5.mincount50.cbow.ns.model" y "es.wiki.size800.window50.mincount50.cbow.ns.model" ponderando por 0.3 el primero y por 0.7 el segundo (ponderar el que mejor va por separado) con SVM sin normalizar!! ##
### NUEVO RECORD -> 74.0% "es.wiki.size800.window5.mincount50.cbow.ns.model" y "es.wiki.size800.window50.mincount50.cbow.ns.model" concatenando vectores!
### NUEVO RECORD -> 80.3% "es.wiki.size800.window5.mincount50.cbow.ns.model" , "es.wiki.size800.window50.mincount50.cbow.ns.model" y "wikipedia.500.window10.mincount50.cbow.ns.model" ponderando por 0.5,0.3,0.2 con la lista de stopwords nueva, SVM sin normalizar. ###
### PROBANDO -> CBOW800(50)+CBOW800(5)+CBOW500, 0.5+0.3+0.2, stopwords3 + svm sin normalizar : MAL ###
#clf  = Config.SUPERVISED_CLASSIFIERS_FOR_TEST["SVM_KERNEL_LINEAR"]
#print Statistics.partition_supervised(train_sentences,test_sentences,clf,model,fnormalization=Utils.normalize_norm_supervised_samples)
#print Statistics.partition_supervised_repr(train_sentences_repr,tuning_sentences_repr,clf)
#print Statistics.partition_supervised_repr(train_sentences_repr,test_sentences_repr,clf)
#repr_train_sentences = FeatureExtractor.get_sentences_representation_supervised(train_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
#repr_test_sentences  = FeatureExtractor.get_sentences_representation_supervised(test_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V)
#print Statistics.partition_supervised_repr_neural_networks(repr_train_sentences,repr_test_sentences,(Config.SIZE_W2V,150,150,73),73)
#############################
#print Statistics.partition_supervised(train_sentences,test_sentences,clf,fnormalization=Utils.normalize_norm_supervised_samples)
### TESTING ALL ###
#model = Word2Vec([sentence for (sentence,cat) in train_sentences],min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V)
#for f in [FeatureExtractor.get_sentence_representation_centroid,FeatureExtractor.get_sentence_representation_sum,FeatureExtractor.get_sentence_representation_centroid_word]:
#for f in [FeatureExtractor.get_sentence_representation_sum,FeatureExtractor.get_sentence_representation_centroid_word]:
#for f in [FeatureExtractor.get_sentence_representation_weighted]: 
#    Config.SENTENCE_REPRESENTATION = f
#    repr_train_sentences = Utils.normalize_norm_supervised_samples(FeatureExtractor.get_sentences_representation_supervised(train_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V))
#    repr_test_sentences  = Utils.normalize_norm_supervised_samples(FeatureExtractor.get_sentences_representation_supervised(test_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V))
#    print "Probando funcion: ",f
#    for key in Config.SUPERVISED_CLASSIFIERS_FOR_TEST:
#	print key," --> ",
#	clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[key]
#	acc = Statistics.partition_supervised_repr(repr_train_sentences,repr_test_sentences,clf) # Leaving one out con algun clasificador de scikit #
#	print str(acc)
###################
"""
# SUPERVISED - considerando que los temas son el nombre de las carpetas - ##
print Messages.supervised_header
print Messages.loading_sentences
sentences = Utils.get_sentences_supervised_from_folders(CORPUS)
print Messages.preprocessing_sentences
sentences = Preprocess.normalize_sentences_supervised(sentences,True,False,False,False,False,False,1)
print Messages.training_w2v
model     = Word2Vec([sentence for (sentence,cat) in sentences],min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V)
print Messages.extracting_features
repr_sentences = FeatureExtractor.get_sentences_representation_supervised(sentences,model,Config.SENTENCE_REPRESENTATION)
Utils.serialize("model_tcstar.m",model)
Utils.serialize("repr_sentences_tcstar.m",repr_sentences)
print "\n\n",Messages.testing_file_header
sentence  = "a pesar de las diferencias que existen entre los diferentes estados tenemos este modelo social europeo que es un modelo particular y que cuenta con el apoyo de las politicas sociales europeas cuyo objetivo es reforzar la fuerza economica europea"
print Messages.preprocessing_sentences
sentence = Preprocess.normalize(sentence,True,False,False,False,True,True,1)
print Messages.extracting_features
sentence = Config.SENTENCE_REPRESENTATION(sentence,model)
print Messages.predicted_class,NeighboursClassifier.get_class(sentence,repr_sentences,1)
print "\n\n",Messages.statistics_resustitution_header
Statistics.resustitution_nearest(sentences,1)
print "\n\n",Messages.statistics_resustitution_header
clf = Config.SUPERVISED_CLASSIFIER()
Statistics.resustitution_supervised_classifier(sentences,clf)
print "\n\n",Messages.statistics_lou_header
Statistics.leaving_one_out_nearest(sentences,1)
print "\n\n",Messages.statistics_lou_header
Statistics.leaving_one_out_supervised_classifier(sentences,clf)
################
print "\n\n"
"""

## CLUSTERING ##
#CORPUS = "./corpusTCstar/training"
#print Messages.clustering_header
#print Messages.loading_sentences
#sentences = Utils.get_sentences_unsupervised_from_folders(CORPUS)
#print Messages.preprocessing_sentences
#sentences = Preprocess.normalize_sentences_unsupervised(sentences,True,False,False,False,True,True,1)
## Con el corpus de tcstar ##
#print Messages.training_w2v
#model     = Word2Vec(sentences,min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V)
#############################
## Con el modelo de la wikipedia ##
#print Messages.training_w2v
#model = Word2Vec.load("es.wiki.w2v.window150.model")
###################################
#print Messages.extracting_features
#repr_sentences = Utils.normalize_norm_unsupervised_samples(FeatureExtractor.get_sentences_representation_unsupervised(sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V))


## Graphic unsupervised ##
#Graphic.plot_unsupervised(repr_sentences)

## Graphic unsupervised (a ver que aprende esto... ilegible) ##
#Config.N_CLUSTERS = 73
#classifier = Config.CLUSTERING_CLASSIFIER(n_clusters=Config.N_CLUSTERS)
#print Messages.extracting_clustering_classes
#classes = ClusteringClassifier.get_classes(classifier,sentences,repr_sentences,3)
#print Messages.extracted_classes,classes
#classes = ClusteringClassifier.get_learnt_classes(classifier,repr_sentences)
#Graphic.plot_supervised(repr_sentences,classes)
#############################################################

## Testing sample with 73 clusters ##
#Config.N_CLUSTERS = 73
#classifier = Config.CLUSTERING_CLASSIFIER(n_clusters=Config.N_CLUSTERS)
#print Messages.extracting_clustering_classes
#classes = ClusteringClassifier.get_classes(classifier,sentences,repr_sentences,1)
#print Messages.extracted_classes,classes
#test_sentence = "el presidente del parlamento europeo"
#print Messages.preprocessing_sentences
#test_sentence = Preprocess.normalize(test_sentence,True,False,False,False,True,True,1)
#test_sentence = Config.SENTENCE_REPRESENTATION(test_sentence,model,Config.SIZE_W2V)
#print Messages.predicted_class,ClusteringClassifier.get_class(classifier,test_sentence,classes)

################
#########################################################################################

############################### DBPEDIA CORPUS ################################
CORPUS_TRAIN = "./corpus_dbpedia/train.csv"
CORPUS_TEST  = "./corpus_dbpedia/test.csv"
CORPUS_CLASSES = "./corpus_dbpedia/classes.txt"

#classes = Utils.get_classes_from_file(CORPUS_CLASSES)
#print Messages.supervised_header
#print Messages.loading_sentences + " (Train) "
#train_sentences = Utils.get_sentences_supervised_from_files_csv(CORPUS_TRAIN,classes)
#print Messages.loading_sentences + " (Test) "
#test_sentences  = Utils.get_sentences_supervised_from_files_csv(CORPUS_TEST,classes)
#print Messages.preprocessing_sentences + " (Train) "
#train_sentences = Preprocess.skflow_tokenizer_supervised(train_sentences)
#print "Unserializing train files"
#train_sentences = Utils.unserialize("train_dbpedia_svc_preprocessed")[0]
#print "Unserializing test files"
#test_sentences  = Utils.unserialize("test_dbpedia_svc_preprocessed")[0]
#Utils.serialize("train_dbpedia_svc_preprocessed",train_sentences)
#print Messages.preprocessing_sentences + " (Test) "
#test_sentences = Preprocess.skflow_tokenizer_supervised(test_sentences)
#Utils.serialize("test_dbpedia_svc_preprocessed",test_sentences)
#logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO) 
#model = Word2Vec([sentence for (sentence,cat) in train_sentences],min_count=Config.MIN_COUNT_W2V,size=Config.SIZE_W2V,window=Config.WINDOW_W2V,workers=Config.WORKERS_W2V,sg=Config.SG_W2V,hs=Config.HS_W2V)
#Utils.serialize("model_w2v_dbpedia_svc_750_hs",model)
#print "Loading w2v model"
#model = Word2Vec.load("en.wiki.w2v.model")
#model = Utils.unserialize("model_w2v_dbpedia_svc_750_hs")[0]
#for f in [FeatureExtractor.get_sentence_representation_sum,FeatureExtractor.get_sentence_representation_centroid_word]:
#    Config.SENTENCE_REPRESENTATION = f
#Config.SENTENCE_REPRESENTATION = FeatureExtractor.get_sentence_representation_centroid_word
#print "Probando funcion: ",Config.SENTENCE_REPRESENTATION
#repr_train_sentences = Utils.normalize_norm_supervised_samples(FeatureExtractor.get_sentences_representation_supervised(train_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V))
#repr_test_sentences  = Utils.normalize_norm_supervised_samples(FeatureExtractor.get_sentences_representation_supervised(test_sentences,model,Config.SENTENCE_REPRESENTATION,Config.SIZE_W2V))
#del train_sentences
#del test_sentences
#del model
#Utils.serialize("repr_train_sentences_w2v_dbpedia_svc_wikipedia_sum",repr_train_sentences)
#Utils.serialize("repr_test_sentences_w2v_dbpedia_svc_wikipedia_sum",repr_test_sentences)
#from random import shuffle
#repr_train_sentences = Utils.unserialize("repr_train_sentences_w2v_dbpedia_svc_wikipedia_sum")[0]
#repr_test_sentences = Utils.unserialize("repr_test_sentences_w2v_dbpedia_svc_wikipedia_sum")[0]
#print "Cargada la representacion"
#shuffle(repr_train_sentences)
#print "Shuffled",len(repr_train_sentences)
#repr_train_sentences = repr_train_sentences[:1500]
#print "Picked ",len(repr_train_sentences)
#repr_test_sentences  = Utils.unserialize("repr_test_sentences_w2v_dbpedia_svc_750_hs_centroid_word")[0]
#X,Y = Graphic.unzip_2d(repr_train_sentences)
#del repr_train_sentences
#Graphic.plot_supervised(X,Y)

#print "Representaciones generadas"
#for key in Config.SUPERVISED_CLASSIFIERS_FOR_TEST:
#    print key," --> ",
#    clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[key]
#    acc = Statistics.partition_supervised_repr(repr_train_sentences,repr_test_sentences,clf) # Leaving one out con algun clasificador de scikit #
#    print str(acc)

############################ UNSUPERVISED DBPEDIA SVC ############################
#repr_train_sentences = Utils.unserialize("repr_train_sentences_w2v_dbpedia_svc_wikipedia_sum")[0]
#repr_train_sentences = [s for (s,c) in repr_train_sentences]
#print "Loaded repr train sentences"
#train_sentences = Utils.unserialize("train_dbpedia_svc_preprocessed_unsupervised")[0]
#print "Loaded train_sentences"
#Config.N_CLUSTERS = 14
#classifier = Config.CLUSTERING_CLASSIFIER(n_clusters=Config.N_CLUSTERS)
#print Messages.extracting_clustering_classes
#classes = ClusteringClassifier.get_classes(classifier,train_sentences,repr_train_sentences,3)
#print Messages.extracted_classes,classes
#del train_sentences
#from random import shuffle
#shuffle(repr_train_sentences)
#print "Suffled repr_train_sentences"
#repr_train_sentences = repr_train_sentences[0:1500]
#classes = ClusteringClassifier.get_learnt_classes(classifier,repr_train_sentences)
#Graphic.plot_supervised(repr_train_sentences,classes)
"""
"""
########################## TESTING TCSTAR TF-IDF ##########################
#CORPUS_TRAIN = "./corpusTCstarJuntado3/training"
#CORPUS_TEST  = "./corpusTCstarJuntado3/testJuntado"

#print Messages.loading_sentences + " (Train) "
#train_sentences = Utils.get_sentences_supervised_from_folders(CORPUS_TRAIN)
#train_sentences = Preprocess.normalize_sentences_supervised(train_sentences,True,False,False,False,True,True,1)
#print Messages.loading_sentences + " (Test) "
#test_sentences  = Utils.get_sentences_supervised_from_folders(CORPUS_TEST)
#test_sentences = Preprocess.normalize_sentences_supervised(test_sentences,True,False,False,False,True,True,1)
#XT,YT = Utils.unzip_2d(train_sentences)
#lT    = len(XT)
#Xt,Yt = Utils.unzip_2d(test_sentences)
#lt    = len(Xt)
#print len(XT),len(Xt)
#XT = Utils.sentences_from_tokenized(XT)
#Xt = Utils.sentences_from_tokenized(Xt)
#all_sentences = XT+Xt
#tfidf_model,tfidf = FeatureExtractor.get_tf_idf_corpus(all_sentences)

#XT = Preprocess.skflow_tokenizer_unsupervised(XT)
#XT          = tfidf[:lT] 
#print XT[0]
#raw_input()
#Xt          = tfidf[lT:]
#print len(XT),len(Xt)
#train_sentences,test_sentences = Utils.zip_2d(XT,YT),Utils.zip_2d(Xt,Yt)
#for key in Config.SUPERVISED_CLASSIFIERS_FOR_TEST:
#	print key," --> ",
#	clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[key]
#	acc = Statistics.partition_supervised_repr(train_sentences,test_sentences,clf) # Leaving one out con algun clasificador de scikit #
#	print str(acc)

### TESTING W2V-TFIDFWeighted ###
#XT,YT = Utils.unzip_2d(train_sentences)
#lT    = len(XT)
#Xt,Yt = Utils.unzip_2d(test_sentences)
#lt    = len(Xt)
#print len(XT),len(Xt)
#XT = Utils.sentences_from_tokenized(XT)
#Xt = Utils.sentences_from_tokenized(Xt)
#all_sentences = XT+Xt
#tfidf_model,tfidf = FeatureExtractor.get_tf_idf_corpus(all_sentences)
#tfidf_dict = FeatureExtractor.get_tf_idf_dict(tfidf_model)
#XT = Preprocess.skflow_tokenizer_unsupervised(XT)
#Xt = Preprocess.skflow_tokenizer_unsupervised(Xt)
#train_sentences = Utils.zip_2d(XT,YT)
#test_sentences  = Utils.zip_2d(Xt,Yt)
#train_sentences  = FeatureExtractor.get_sentences_representation_supervised(train_sentences,model,FeatureExtractor.get_sentence_representation_w2v_tfidf,Config.SIZE_W2V,tfidf_dict)
#test_sentences  = FeatureExtractor.get_sentences_representation_supervised(test_sentences,model,FeatureExtractor.get_sentence_representation_w2v_tfidf,Config.SIZE_W2V,tfidf_dict)
##### TESTING CLASSIFIERS #######
#for key in Config.SUPERVISED_CLASSIFIERS_FOR_TEST:
#    print key," --> ",
#    clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[key]
#    acc = Statistics.partition_supervised_repr(train_sentences,test_sentences,clf,fnormalization=Utils.normalize_norm_supervised_samples)
#    print str(acc)
#################################

## PLOT ##
#X,Y = Graphic.unzip_2d(Utils.normalize_norm_supervised_samples(train_sentences))
#print "PLOTTING..."
#Graphic.plot_supervised(X,Y)
##########


##### CLUSTERING TF-IDF #####
#classifier = Config.CLUSTERING_CLASSIFIER(n_clusters=Config.N_CLUSTERS)
## Testing graphics ##
#classes = ClusteringClassifier.get_learnt_classes(classifier,repr_XT)
#Graphic.plot_supervised(repr_XT[:5000],classes[:5000])
## Testing classify clustering topic by k-gram ##
#print Messages.extracting_clustering_classes
#classes = ClusteringClassifier.get_classes(classifier,XT,repr_XT,1)
#print Messages.extracted_classes,classes
######################

###########################################################################################################################################
#							 Doc2Vec
###########################################################################################################################################
#from gensim.models import Doc2Vec
#from gensim.models.doc2vec import LabeledSentence
#CORPUS_TRAIN     = "./corpusTCstarJuntado2/training"
#CORPUS_TEST      = "./corpusTCstarJuntado2/testJuntado"
#train_sentences  = Utils.get_sentences_supervised_from_folders(CORPUS_TRAIN)
#test_sentences   = Utils.get_sentences_supervised_from_folders(CORPUS_TEST)
#train_sentences  = Preprocess.normalize_sentences_supervised(train_sentences,True,False,False,False,True,True,1)
#test_sentences   = Preprocess.normalize_sentences_supervised(test_sentences,True,False,False,False,True,True,1)

## Generate model ##
"""
i = 0
train_labeled_sentences = []
for (s,c) in train_sentences:
    train_labeled_sentences.append(LabeledSentence(s,tags=[str(i)]))
    i += 1
print "End"
model 		= Doc2Vec(size=150, window=8, min_count=50, workers=1, dm=0, hs=0, dbow_words=0, dm_concat=0) # use fixed learning rate
model.build_vocab(train_labeled_sentences)
for epoch in range(10):
    print "Epoch:",epoch
    model.train(train_labeled_sentences)
    model.alpha -= 0.002  # decrease the learning rate
    model.min_alpha = model.alpha  # fix the learning rate, no decay
model.save("test.doc2vec")
"""
#####################
#model = Doc2Vec.load('test.doc2vec')
#model = Doc2Vec.load('test.wiki.2.model')
#print len(model.docvecs)
## Corpus Wiki ##
#repr_train_sentences = [(model.infer_vector(s),c) for (s,c) in train_sentences]
#repr_test_sentences  = [(model.infer_vector(s),c) for (s,c) in test_sentences]
#################
## Corpus Model ##
#repr_train_sentences = [(model.docvecs[str(i)],train_sentences[i][1]) for i in xrange(len(model.docvecs))]
#repr_test_sentences  = [(model.infer_vector(s),c) for (s,c) in test_sentences]
##################
#clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST["KNN_1"]
#print Statistics.partition_supervised_repr(repr_train_sentences,repr_test_sentences,clf,fnormalization=Utils.normalize_norm_supervised_samples)
