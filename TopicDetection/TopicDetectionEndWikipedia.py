#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.insert(0, '../')
from Kernel import Config
from Kernel import Preprocess
from Kernel import NeighboursClassifier
from Kernel import ClusteringClassifier
from Kernel import FeatureExtractor
from Kernel import Optimizer
from Kernel import Statistics
from Kernel import Graphic
from Kernel import Utils
from Kernel import Messages
from gensim.models import Word2Vec
from gensim.models import Doc2Vec
from numpy import concatenate
import logging


if Config.VERBOSE: logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
 
CORPUS_TRAIN  = "./corpusTCstarTuning/training"
CORPUS_TUNING = "./corpusTCstarTuning/tuning"
print Messages.supervised_header
print Messages.loading_sentences + " (Train) "
train_sentences = Utils.get_sentences_supervised_from_folders(CORPUS_TRAIN)
print Messages.loading_sentences + " (Tuning) "
tuning_sentences  = Utils.get_sentences_supervised_from_folders(CORPUS_TUNING)
print Messages.preprocessing_sentences + " (Train) "
train_sentences = Preprocess.normalize_sentences_supervised(train_sentences,True,False,False,False,True,True,1)
print Messages.preprocessing_sentences + " (Tuning) "
tuning_sentences  = Preprocess.normalize_sentences_supervised(tuning_sentences,True,False,False,False,True,True,1)

## Barrido sobre modelos de TC-STAR ##
size    = [100,200,300,400,500,600,700,800,900]
window  = [5,10,20,50,100,150]
for s in size:
    for w in window:
	try: 
	    model = Word2Vec.load("./BarridoParametros/TCStar/tcstar_size_"+str(s)+"_window_"+str(w)+".model")
	    for rep in ["suma","centroide"]:
		if rep=="suma": r = FeatureExtractor.get_sentence_representation_sum
		elif rep=="centroide": r = FeatureExtractor.get_sentence_representation_centroid
		train_sentences_repr  = FeatureExtractor.get_sentences_representation_supervised(train_sentences,model,r,s)
		tuning_sentences_repr = FeatureExtractor.get_sentences_representation_supervised(tuning_sentences,model,r,s)
		for clf_name in ["SVM_KERNEL_LINEAR","KNN_1"]:
		    clf = Config.SUPERVISED_CLASSIFIERS_FOR_TEST[clf_name]
		    print "Wiki size",s,"window",w,"rep",rep,"classifier",clf_name,"accuracy:",Statistics.partition_supervised_repr(train_sentences_repr,tuning_sentences_repr,clf)
	except: continue
######################################
