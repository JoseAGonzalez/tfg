#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.insert(0, '../')
from Kernel import Config
from Kernel import Utils
from Kernel import Preprocess
from gensim.models import Doc2Vec
from random import randint
from gensim.models.doc2vec import LabeledSentence
import logging
CORPUS_TRAIN    = "./corpusTCstarJuntado2/training"
CORPUS_TEST     = "./corpusTCstarJuntado2/testJuntado"
train_sentences = Utils.get_sentences_supervised_from_folders(CORPUS_TRAIN)
test_sentences  = Utils.get_sentences_supervised_from_folders(CORPUS_TEST)
train_sentences  = Preprocess.normalize_sentences_supervised(train_sentences,True,False,False,False,True,True,1)
test_sentences  = Preprocess.normalize_sentences_supervised(test_sentences,True,False,False,False,True,True,1)
## Generate model ##
#i = 0
#train_labeled_sentences = []
#for (s,c) in train_sentences:
#    train_labeled_sentences.append(LabeledSentence(s,tags=[str(i)]))
#    i += 1
#print "End"
#model 		= Doc2Vec(size=300, window=150, min_count=1, workers=1,alpha=0.025, min_alpha=0.025) # use fixed learning rate
#model.build_vocab(train_labeled_sentences)
#for epoch in range(10):
#    print "Epoch:",epoch
#    model.train(train_labeled_sentences)
#    model.alpha -= 0.002  # decrease the learning rate
#    model.min_alpha = model.alpha  # fix the learning rate, no decay
#model.save("test.doc2vec")
#####################
model = Doc2Vec.load('test.doc2vec')
repr_train_sentences = [(model.docvecs[i],train_sentences[i][1]) for i in xrange(len(model.docvecs))]
repr_test_sentences  = [(model.infer_vector(s),c) for (s,c) in test_sentences]

#sentence = LabeledSentence([u'debates',u'evento'],[u'SENT_1'])
#print model.infer_vector([u'debates',u'evento'])

