#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import logging
sys.path.insert(0, '../')
from Kernel import Config
from Kernel import Preprocess
from Kernel import NeighboursClassifier
from Kernel import ClusteringClassifier
from Kernel import FeatureExtractorDBpediaProject
from Kernel import FeatureExtractorDBpediaSpotlight
from Kernel import Optimizer
from Kernel import StatisticsDBpedia
from Kernel import Utils
from Kernel import Messages

p = Utils.unserialize("properties.dump")[0]
print p
